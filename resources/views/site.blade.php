<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('style/images/logo/logo-white.png') }}" />
    <title>E-Kids Villa</title>
    <link href="{{ asset('style/css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <site-app>
    </site-app>
</div>

<script>
    window.surl = "{{ url('/') }}";
</script>
<script src="{{ asset('style/js/app.js') }}"></script>
<script src="{{ asset('style/tinymce/tinymce.min.js')}}"></script>
</body>
</html>
