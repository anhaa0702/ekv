@component('mail::message')
# Сайн байна уу {{$user->firstname}}
Эрхэм харилцагч танд энэ өдрийн мэнд хүргэе!
Таны хүсэлтийг хүлээн авлаа.

Нууц үг: {{$genpass}}

@component('mail::panel', ['url' => ''])
Энэхүү нууц үг нь зөвхөн нэг удаа хэрэглэгдэхийг анхаарна уу.
@endcomponent

Regards, <br>
{{ config('app.name') }}
@endcomponent
