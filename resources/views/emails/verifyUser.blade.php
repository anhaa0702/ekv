@component('mail::message')
# Сайн байна уу {{$user['firstname']}}
ekv.mn вэб сайтад бүртгүүлсэнд баярлалаа. Та "Баталгаажуулах" товчин дээр дарж email ээ баталгаажуулна уу.

@component('mail::button', ['url' => url('user/verify', $user->verifyUser->token), 'color' => 'green'])
Баталгаажуулах
@endcomponent

@component('mail::panel', ['url' => ''])
Дээрх товч ажиллахгүй бол <a href="{{url('user/verify', $user->verifyUser->token)}}">Энд</a> дарна уу.
@endcomponent

Regards, <br>
{{ config('app.name') }}
@endcomponent
