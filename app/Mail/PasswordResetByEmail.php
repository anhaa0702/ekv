<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class PasswordResetByEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $genpass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->genpass = Str::random(6).''.Str::upper(Str::random(2)).''.Str::lower(Str::random(2)).''.date('s').'';
        $this->user->password = bcrypt($this->genpass);
        $this->user->save();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.resetPassword');
    }
}
