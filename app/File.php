<?php
namespace App;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    public static function file_delete($file){
        Storage::delete($file);
    }

    public static function file($request, $dir){
        if(!$request->hasFile('file')){
            return null;
        }
        $file = $request->file('file')->store($dir);
        return $file;
    }

    public static function deleteImg($file){
        Storage::delete($file);
    }

    public static function upload($request){
        $img = $request->file->store('');
        $save = Image::make('upload/'.$img);

        // $save->orientate();
        // $save->resize(510, null, function ($constraint) { $constraint->aspectRatio(); } );
        // $save->save('upload/'.$img);

        return $img;
    }


}
