<?php
namespace App;
use Laravel\Passport\HasApiTokens;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


use App\Modal\User\UserProvider;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    use HasApiTokens;
    protected $table="users";
    protected $fillable = [
        'id',
        'login_name',
        'avatar',
        'firstname',
        'lastname',
        'rd',
        'birthday',
        'gender',
        'password',
        'isCheckTerm',
        'isActive',
        'activated',
        'isSuperAdmin', 
        'end_at',
        'phone'
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function change($data){
        if ($data['id']> 0) {
            $row = User::find($data['id']);
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
            if($row->save()){
                return $row;
            }
        }
        return false;
    }

    public function company()
    {
        return $this->belongsToMany(Company::class, CompanyUser::class)->get(array('company.*','CompanyUser.role as company_role'));
    }

    public function providers()
    {
        return $this->hasMany(UserProvider::class, 'user_id');
    }
    
}
