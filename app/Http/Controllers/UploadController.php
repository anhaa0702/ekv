<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\File;

class UploadController extends Controller{

    public function Image(Request $request){
        $image=File::upload($request);
        $json['name']=$image;
        $json['status']='done';
        $json['url']=url('upload/' . $image);
        return response()->json($json);
    }
    
    public function File(Request $request){
        $fileName = time().'.'.$request->file->extension();  
   
        $request->file->move(public_path('uploads'), $fileName);

        $json['name']=$fileName;
        $json['status']='done';
        $json['url']=url('upload/' . $fileName);
        return response()->json($json);
    }
}