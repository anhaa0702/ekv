<?php
namespace App\Http\Controllers\ConstantData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\Modal\ConstantData\CountryModal;
use App\Modal\ConstantData\SettingsModal;
use App\Modal\ConstantData\BannerModal;
use App\Modal\Content\ContentType;

class ConstCtrl extends Controller{

  public function city(){
    $datas=CountryModal::where("country_type", 2)->select('id','name')->get();
    return response()->json(['status'=>true, 'data'=>$datas]);
  }

  public function children($id){
      $datas=CountryModal::where("country_id", $id)->select('id','name')->get();
      return response()->json(['status'=>true, 'data'=>$datas]);
  }

  public function const_by_key(Request $request){
    $data = $request->get('data');
    $data = json_decode($data, true);
    return response()->json(['is_done'=>true, 'data'=>SettingsModal::by_key($data['key']),'message'=>'Амжилттай.'], 200);  
  }

  public function banners(Request $request){
    $data = $request->get('data');
    $data = json_decode($data, true);
    return response()->json(['is_done'=>true, 'data'=>BannerModal::by_key($data['key']),'message'=>'Амжилттай.'], 200);  
  }

  public function menu(){
    $result = ContentType::where('mainMenu', '1')->with('children')->orderBy('pIndex','ASC')->get();
    return response()->json(['is_done'=>true, 'data'=>$result,'message'=>'Амжилттай.'], 200);  
  }
}