<?php
namespace App\Http\Controllers\Loyalty;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\Modal\Loyalty\Company;
use App\Modal\Loyalty\CompanyType;


class CompanyTypeController extends Controller{
  public function index()
  {
    $result = CompanyType::orderBy('name', 'asc')->get();
    return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$result], 200);
  }

  public function store(Request $request)
  {
      $data = $request->get('data');
      $result = CompanyType::change(json_decode($data, true));
      return response()->json(['data' => $result, 'is_done'=>$result ? true : false, 'message'=> $result ? 'Амжилттай' : 'Алдаа гарлаа дахин оролдоно уу!']);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id){}  public function edit($id){}

  public function update(Request $request, $id){}

  public function destroy($id){}
}