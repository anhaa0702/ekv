<?php
namespace App\Http\Controllers\Loyalty;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\User;
use App\Modal\User\UserCompanies;
use App\Modal\Loyalty\CompanyType;
use App\Modal\Loyalty\Company;

class CompanyController extends Controller{
  public function index()
  {
    $result = Company::orderBy('created_at', 'desc')->paginate(30);
    return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$result], 200);
  }

  public function show($id){
    $result = Company::where('id',$id)->with('city', 'district', 'ward','type','main')->get();
    $result = count($result) >0 ? $result[0] : null;

    $result['user_company'] = null;
    if(Auth::user()){
      $result['user_company'] = UserCompanies::where('user_id', Auth::user()->id)->where('company_id', $id)->where('status', 'active')->first();
    }
    return response()->json(['is_done' => true, 'data' => $result]);
  }

  public function store(Request $request){
    $data = $request->get('data');
    $data = json_decode($data, true);
    return response()->json(['is_done'=>true, 'data'=>Company::change($data),'message'=>'Амжилттай.'], 200);
  }
  
  public function destroy($id){}
}