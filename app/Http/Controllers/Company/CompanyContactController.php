<?php
namespace App\Http\Controllers\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\User;
use App\Modal\Company\Contact;
use App\Modal\Company\Company;

class CompanyContactController extends Controller{
  public function index()
  {
    extract(request()->only(['id']));
    $result = Contact::where('company_id', $id)->orderBy('created_at', 'desc')->get();
    return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$result], 200);
  }

  public function store(Request $request){
    $r =  Contact::change(json_decode($request->get('data'), true));
    return response()->json(['is_done'=>$r ? true : false, 'data'=>$r, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
  }

  public function destroy($id)
  {
      $r =  Contact::drop($id);
      return response()->json(['is_done'=>$r ? true : false, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
  }
}