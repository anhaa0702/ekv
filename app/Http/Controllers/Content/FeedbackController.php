<?php
namespace App\Http\Controllers\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model


class FeedbackController extends Controller{
  public function index(){}
  public function create(){}
  public function store(Request $request){}
  public function show($id){}
  public function edit($id){}
  public function update(Request $request, $id){}
  public function destroy($id){}
}
