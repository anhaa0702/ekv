<?php
namespace App\Http\Controllers\Content;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Content\Post;
use App\Modal\Content\PostLike;
use App\Modal\Content\PostSave;
use App\Modal\Content\PostSee;
use App\Modal\Content\PostComments;

class CommentController extends Controller{
    
    public function index(Request $request){
        extract(request()->only(['page', 'post_id', 'user_id']));

        if(Auth::user()){
            $result = PostComments::where('post_id', $post_id);
            $result = $result->with('user')->orderby('created_at', 'desc')->paginate(50);
            return response()->json(['is_done'=>false, 'data'=>$result,'message'=>'Нэвтэрнэ үү.'], 200);
        }else{
            return response()->json(['is_done'=>false, 'data'=>null,'message'=>'Нэвтэрнэ үү.'], 200);
        }
    }

    public function store(Request $request){
        $data = $request->get('data');
        $data = json_decode($data, true);
        return response()->json(['is_done'=>true, 'data'=>PostComments::change($data),'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        $result = PostComments::where('id',$id)->with('user')->get()[0];
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }
    public function destroy($id)
    {
        try {
            $row = PostComments::find($id);
            $row->deleted_at = Carbon::now();
            $row->save();
            return response()->json(['is_done' => true, 'message'=>'Амжилттай.']);
        } catch (\Exception $exception) {
            return response()->json(['is_done' => false, 'message'=>'Устгагдсан байна!']);
        }
    }
}
