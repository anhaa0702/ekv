<?php
namespace App\Http\Controllers\Content;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Content\ContentType;
use App\Modal\Content\Content;

class ContentTypeController extends Controller
{
    public function index(Request $request){
        extract(request()->only(['is_special']));

        if(isset($is_special) && $is_special ==1){
            $result= ContentType::where('is_special','1')->with('contents')->orderby('name', 'asc')->get();
        }else{
            $result= ContentType::withCount('contents')->orderby('name', 'asc')->get();
        }
        
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }
}