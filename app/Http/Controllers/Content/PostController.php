<?php
namespace App\Http\Controllers\Content;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Content\Post;
use App\Modal\Content\PostLike;
use App\Modal\Content\PostSave;
use App\Modal\Content\PostSee;

class PostController extends Controller
{
    public function index(Request $request){
        extract(request()->only(['page', 'post_type', 'company_id','only_companies']));

        $result= Post::where('id','>',0);
        if(isset($only_companies) && $only_companies == true || isset($company_id) && $company_id>0){
            $result= isset($company_id) && $company_id>0 ? Post::where('company_id', $company_id) :  Post::where('company_id','>', 0);
        }

        if(isset($post_type)){
            $result= $result->where('post_type', $post_type);
        }
        $result = $result->with('user','company','files','comments')->orderby('created_at', 'desc')->paginate(50);
        return response()->json($result);
    }

    public function show($id)
    {
        if(Auth::user() && $id>0){
            $r = new PostSee;
            $r->post_id = $id;
            $r->user_id = Auth::user()->id;
            $r->save();
            
            $p = Post::find($id);
            $p->see = $p->see + 1;
            $p->save();
        }
        $result = Post::where('id',$id)->with('user','company','files','comments')->get()[0];
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $data = $request->get('data');
        $data = json_decode($data, true);
        return response()->json(['is_done'=>true, 'data'=>Post::change($data),'message'=>'Амжилттай.'], 200);
    }

    public function action($type, $id){
        if(Auth::user() && $id>0){
            $p = Post::find($id);
            if($type == 'save'){
                if( PostSave::where('user_id', Auth::user()->id)->where('post_id', $id)->count()>0){
                    PostSave::where('user_id', Auth::user()->id)->where('post_id', $id)->delete();
                    if($p->save>0){
                        $p->save = $p->save - 1;
                    }
                }else{
                    $r = new PostSave;
                    $r->post_id = $id;
                    $r->user_id = Auth::user()->id;
                    $r->save();
                    $p->save = $p->save +1;
                }
            }

            if($type == 'like'){
                if( PostLike::where('user_id', Auth::user()->id)->where('post_id', $id)->count()>0){
                    PostLike::where('user_id', Auth::user()->id)->where('post_id', $id)->delete();
                    if($p->like>0){
                        $p->like = $p->like - 1;
                    }
                }else{
                    $r = new PostLike;
                    $r->post_id = $id;
                    $r->user_id = Auth::user()->id;
                    $r->save();
                    $p->like = $p->like +1;
                }
            }

            $r = new PostSee;
            $r->post_id = $id;
            $r->user_id = Auth::user()->id;
            $r->save();

            $p->see = $p->see + 1;
            $p->save();
            return response()->json(['is_done' => true, 'message'=>'Амжилттай.']);
        }
        return response()->json(['is_done' => false, 'message'=>'Нэвтэрнэ үү!']);
    }

    public function destroy($id)
    {
        try {
            $row = Post::find($id);
            $row->deleted_at = Carbon::now();
            $row->save();
            return response()->json(['is_done' => true, 'message'=>'Амжилттай.']);
        } catch (\Exception $exception) {
            return response()->json(['is_done' => false, 'message'=>'Устгагдсан байна!']);
        }
    }
}