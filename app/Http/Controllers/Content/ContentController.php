<?php
namespace App\Http\Controllers\Content;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modal\Content\ContentType;
use App\Modal\Content\Content;

class ContentController extends Controller
{
    public function index(Request $request){
        extract(request()->only(['type_id']));

        $result= [];
        
        if(isset($type_id) && $type_id>0 && $type_id!=null){
            $ids =  Content::getIdsByType($type_id);
            $result = Content::whereIn('id',$ids)->with('types','authors');
        }else{
            $result = Content::with('types','authors');
        }

        $result = $result->select('id','logo','title','short_desc','see','created_at')->orderby('created_at', 'desc')->paginate(10);
        return response()->json($result);
    }

    public function show($id)
    {
        if($id>0) {
            $p = Content::find($id);
            $p->see = $p->see + 1;
            $p->save();
        }
        $result = Content::where('id',$id)->with('types','authors')->get()[0];
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }
}