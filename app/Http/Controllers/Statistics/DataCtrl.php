<?php
namespace App\Http\Controllers\Statistics;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// model
use App\User;
use App\Modal\Loyalty\CompanyType;
use App\Modal\Loyalty\Company;

class DataCtrl extends Controller{

    public function counter(){
        // гишүүд
        $data['user'] = User::count();
        return response()->json(['is_done'=>true, 'data'=>$data,'message'=>'Амжилттай.'], 200);
    }

    public function partners(){
        return response()->json(['is_done'=>true, 'data'=> Company::where('partnership','1')->get(),'message'=>'Амжилттай.'], 200);
    }

}