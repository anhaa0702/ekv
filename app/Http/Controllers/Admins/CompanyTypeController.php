<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Company\Type;

class CompanyTypeController extends Controller
{
    public function index(){
        extract(request()->only(['no_paging','query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = [];
        if(isset($no_paging) && $no_paging){
            $result = Type::orderBy('name', 'ASC')->get();
        }else{
            $result = (isset($orderBy))  ? Type::orderBy($orderBy, $ascending == 1 ? 'ASC' : 'DESC') : Type::orderBy('created_at', 'desc');
            $result = $result->with('companies')->paginate($limit);
        }
        return response()->json(['is_done'=>true, 'data'=>$result, 'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        $result =  Type::get($id);
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $r =  Type::change(json_decode($request->get('data'), true));
        return response()->json(['is_done'=>$r ? true : false, 'data'=>$r, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }

    public function destroy($id)
    {
        $r =  Type::drop($id);
        return response()->json(['is_done'=>$r ? true : false, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }
}