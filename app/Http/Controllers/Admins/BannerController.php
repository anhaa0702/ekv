<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;


use App\Modal\ConstantData\BannerModal;

class BannerController extends Controller
{
    public function index(){
        if(!Auth::user() || !(Auth::user() && Auth::user()->isSuperAdmin)){
            return response()->json(['is_done'=>false, 'message'=>'Хандах эрх байхгүй байна!!!'], 200);
        }

        extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = BannerModal::where('id', '>', 0);

        if (isset($orderBy)) {
            $direction = $ascending == 1 ? 'ASC' : 'DESC';
            $result = $result->orderBy($orderBy, $direction);
        }else{
            $result = $result->orderBy('created_at', 'desc');
        }

        $result = $result->paginate($limit);
        return response()->json(['is_done'=>true, 'data'=>$result,'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        if(!Auth::user() || !(Auth::user() && Auth::user()->isSuperAdmin)){
            return response()->json(['is_done'=>false, 'message'=>'Хандах эрх байхгүй байна!!!'], 200);
        }

        $result = BannerModal::find($id);
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        if(!Auth::user() || !(Auth::user() && Auth::user()->isSuperAdmin)){
            return response()->json(['is_done'=>false, 'message'=>'Хандах эрх байхгүй байна!!!'], 200);
        }

        $data = $request->get('data');
        $data = json_decode($data, true);
        return response()->json(['is_done'=>true, 'data'=>BannerModal::change($data),'message'=>'Амжилттай.'], 200);
    }

    public function destroy($id)
    {
        if(!Auth::user() || !(Auth::user() && Auth::user()->isSuperAdmin)){
            return response()->json(['is_done'=>false, 'message'=>'Хандах эрх байхгүй байна!!!'], 200);
        }
        
        try {
            $category = BannerModal::find($id)->delete();
            return response()->json(['is_done' => true]);
        } catch (\Exception $exception) {
            return response()->json(['is_done' => false]);
        }
    }
}
                                                            