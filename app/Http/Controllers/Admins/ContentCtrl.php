<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Content\Content;

class ContentCtrl extends Controller
{
    public function index(){
        extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = (isset($orderBy))  ? Content::orderBy($orderBy, $ascending == 1 ? 'ASC' : 'DESC') : Content::orderBy('created_at', 'desc');
        if (isset($query) && $query) {
            $result = $byColumn == 1 ? $this->filterByColumn($result, $query) :  $this->filter($result, $query, ['title']);
        }

        $result = $result->with('types','authors')->paginate($limit);
        return response()->json(['is_done'=>true, 'data'=>$result, 'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        $result =  Content::get($id);
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $r =  Content::change(json_decode($request->get('data'), true));
        return response()->json(['is_done'=>$r ? true : false, 'data'=>$r, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }

    public function destroy($id)
    {
        $r =  Content::drop($id);
        return response()->json(['is_done'=>$r ? true : false, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }

    protected function filterByColumn($data, $queries)
    {
        $queries = json_decode($queries);
        return $data->where(function ($q) use ($queries) {
            foreach ($queries as $field => $query) {
                if (is_string($query)) {
                    $q->whereRaw("UPPER(" . $field . ") LIKE '%" . mb_strtoupper($query) . "%'");
                }
            }
        });
    }
}