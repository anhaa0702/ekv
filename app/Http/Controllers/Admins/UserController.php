<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\Modal\Company\Type;
use App\Modal\Company\Company;
use App\User;

class UserController extends Controller
{
    public function index(){
        extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = (isset($orderBy))  ? User::orderBy($orderBy, $ascending == 1 ? 'ASC' : 'DESC') : User::orderBy('created_at', 'desc');
        $result = $result->with('providers')->paginate($limit);
        return response()->json(['is_done'=>true, 'data'=>$result,'message'=>'Амжилттай.'], 200);
    }

    public function findByName($value)
    {
        $result = User::orderBy('id', 'ASC')->limit(5)->get();
        if($value && $value!="null" && $value!=null){
            $result = User::where('name','like', '%' . $value.'%' )->where('main_id','<', 1)->limit(5)->get();
        }
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function show($id)
    {
        $result = User::find($id);
        $result->main = $result->main_id>0 ? User::find($result->main_id) : "";
        // $result->type = $result->type_id>0 ? Type::find($result->type_id) : "";
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $data = $request->get('data');
        $data = json_decode($data, true);
        return response()->json(['is_done'=>true, 'data'=>User::change($data),'message'=>'Амжилттай.'], 200);
    }

    public function destroy($id)
    {
        try {
            $category = User::find($id);
            $category->deleted_at = Carbon::now();
            $category->save();
            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false]);
        }
    }
}