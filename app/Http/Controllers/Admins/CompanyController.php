<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Company\Company;

class CompanyController extends Controller
{
    public function index(){
        extract(request()->only(['query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = (isset($orderBy))  ? Company::orderBy($orderBy, $ascending == 1 ? 'ASC' : 'DESC') : Company::orderBy('created_at', 'desc');
        $result = $result->with('main', 'types')->paginate($limit);
        return response()->json(['is_done'=>true, 'data'=>$result, 'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        $result =  Company::get($id);
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $r =  Company::change(json_decode($request->get('data'), true));
        return response()->json(['is_done'=>$r ? true : false, 'data'=>$r, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }

    public function destroy($id)
    {
        $r =  Company::drop($id);
        return response()->json(['is_done'=>$r ? true : false, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }
}