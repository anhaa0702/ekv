<?php
namespace App\Http\Controllers\Admins;
use App\Services\PayUService\Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modal\Content\ContentAuthor;

class ContentAuthorCtrl extends Controller
{
    public function index(){
        extract(request()->only(['no_paging','content_id', 'query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn']));
        $result = [];
        if(isset($no_paging) && $no_paging){
            $result = ContentAuthor::orderBy('full_name', 'ASC')->get();
        }else{
            $result = (isset($orderBy))  ? ContentAuthor::orderBy($orderBy, $ascending == 1 ? 'ASC' : 'DESC') : ContentAuthor::orderBy('created_at', 'desc');
            $result = $result->paginate($limit);
        }
        return response()->json(['is_done'=>true, 'data'=>$result, 'message'=>'Амжилттай.'], 200);
    }

    public function show($id)
    {
        $result =  ContentAuthor::get($id);
        return response()->json(['data' => $result, 'is_done'=>true, 'message'=> 'Амжилттай']);
    }

    public function store(Request $request){
        $r =  ContentAuthor::change(json_decode($request->get('data'), true));
        return response()->json(['is_done'=>$r ? true : false, 'data'=>$r, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }

    public function destroy($id)
    {
        $r =  ContentAuthor::drop($id);
        return response()->json(['is_done'=>$r ? true : false, 'message'=>$r ? 'Амжилттай.' : 'Алдаа гарлаа дахин оролдно уу.'], 200);
    }
}