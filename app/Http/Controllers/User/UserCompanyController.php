<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

// model
use App\User;
use App\Modal\User\UserCompanies;

class UserCompanyController extends Controller{
 
  public function index()
  {
    if(Auth::user()){
      $id = Auth::user()->id;
      $datas = UserCompanies::gets($id);
      return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$datas], 200);
    }
    return response()->json(['is_done'=>false, 'message'=>'Нэвтэрнэ үү.', 'data'=>$datas], 200);
  }

  // нэг хаягийн мэдээлэл засах
  public function store(Request $request)
  {
      $data = $request->get('data');
      $result = UserCompanies::change(json_decode($data, true));
      return response()->json(['data' => $result, 'is_done'=>$result ? true : false, 'message'=> $result ? 'Амжилттай' : 'Алдаа гарлаа дахин оролдоно уу!']);
  }

  // устсан төлөвт оруулах
  public function destroy($id)
  { 
    $row = UserCompanies::find($id);
    $row->deleted_at = Carbon::now();
    $uId = $row->user_id;
    $row->save();
    return response()->json(['is_done' => true, 'message' => 'Амжилттай устгагдлаа.', 'data'=> UserCompanies::gets($uId) ]);
  }
}