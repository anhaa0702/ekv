<?php
namespace App\Http\Controllers\User;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;

// model
use App\Modal\User\UserProvider;
use App\Modal\User\UserAddress;
use App\Modal\User\UserCompany;
use App\User;

class UserController extends Controller{
  public function index()
  {
    // $result = User::orderBy('created_at', 'desc')->paginate(30);
    // return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$result], 200);
  }


  public function getByUser($user_id)
  {
    // $result = User::company($user_id);
    // return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.', 'data'=>$result], 200);
  }
  
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(){}

  public function store(Request $request)
  {
    $params_data = $request->get('data');
    $data = json_decode($params_data, true);

    $user = User::find($data['id']);
    $validator = Validator::make($data, [
        'login_name' =>['required', Rule::unique('users')->ignore($user->id)],
        'rd' => ['required',  Rule::unique('users')->ignore($user->id)]
        ]);

    if ($validator->fails()) {
        return response()->json(['status'=> 1, 'message'=>$validator->errors()->first(), 'data'=> $data]);
    }
    
    $result = User::change($data);
    return response()->json(['is_done' => true,'data'=>$result ,'message'=> $result ? 'Амжилттай' : 'Алдаа гарлаа дахин оролдоно уу!']);
  }

  public function show($id)
  {
    $user = User::find($id);
    $user->avatar = $user->avatar ? $user->avatar : env('APP_URL') . "/male.png";
    $user['contacts'] = UserProvider::gets($user->id);
    $user['addresses'] = UserAddress::gets($user->id);
    $user['companies'] = UserCompanies::gets($user->id);
    return response()->json(['is_done'=>true, 'message'=>'Амжилттай.', 'data'=> $user], 200);
  }

  public function edit($id){}

  public function update(Request $request, $id){}

  public function destroy($id){}
}