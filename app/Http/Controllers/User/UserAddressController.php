<?php
namespace App\Http\Controllers\User;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;

// model
use App\User;
use App\Modal\User\UserAddress;


class UserAddressController extends Controller{
  

  // нэг хаягийн мэдээлэл засах
  public function store(Request $request)
  {
      $data = $request->get('data');
      $result = UserAddress::change(json_decode($data, true));
      return response()->json(['data' => $result, 'is_done'=>true, 'message'=> $result ? 'Амжилттай' : 'Алдаа гарлаа дахин оролдоно уу!']);
  }

  // устсан төлөвт оруулах
  public function destroy($id)
  {
    UserAddress::delete($id);
    return response()->json(['is_done'=>true, 'msg' => 'Амжилттай устгагдлаа.']);
  }
}