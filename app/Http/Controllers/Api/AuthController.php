<?php
namespace App\Http\Controllers\Api;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Services\PayUService\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
use Hash;

use App\Modal\User\UserProvider;
use App\Modal\User\UserAddress;
use App\User;
class AuthController extends Controller
{
    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json(['success' => 'Амжилттай.'], 200);
    }

    public function login(Request $request) {
        $i =  $request->all();
        if (!isset($i['login_name']) || !isset($i['password'])){
            return response()->json(['is_done'=>false, 'message'=>'Нэвтрэх нэр эсвэл нууц үг таарахгүй байна'], 200);
        }
        $attempt['password']= $i['password'];
        $attempt['login_name']= $i['login_name'];
        if (Auth::attempt($attempt)) {
            $user = Auth::user();
            $token = $user->createToken('ekidsvilla')->accessToken;
            return response()->json(['is_done' =>true, 'token'=>$token, 'message'=>'Амжилттай нэвтэрлээ.'], 200);
        }
        return response()->json(['is_done'=>false, 'message'=>'Нэвтрэх нэр эсвэл нууц үг таарахгүй байна'], 200);
    }

    public function register(Request $request) {
        $params_data = $request->get('data');
        $data = json_decode($params_data, true);

        $validator = Validator::make($data, [
            'login_name' => 'required|unique:users',
            'firstname' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['is_done'=> false, 'message'=>$validator->errors()->first()], 200);
        }
        $data['password'] = bcrypt($data['password']);
        $row = new User($data);
        if(!$row->save()){
            return response()->json(['is_done'=> false, 'message'=>'Хэрэглэгчийн мэдээлэл хадгалахад алдаа гарлаа!'], 200);
        }

        if(isset($data['phone'])){
            $p = new UserProvider;
            $p->providers="phone";
            $p->user_id=$row->id;
            $p->value=$data['phone'];
            $p->save();
        }

        if(isset($data['email'])){
            $p = new UserProvider;
            $p->providers="email";
            $p->user_id=$row->id;
            $p->value=$data['email'];
            $p->save();
        }
        return response()->json(['is_done'=>true, 'message'=>'Амжилттай бүртгэгдлээ.'], 200);
    }

    public function user() {
        $user = Auth::user();
        $user['avatar'] = $user->avatar ? $user->avatar : env('APP_URL') . "/male.png";
        $user['contacts'] = UserProvider::gets($user->id);
        $user['addresses'] = UserAddress::gets($user->id);
        return response()->json($user);
    }
}