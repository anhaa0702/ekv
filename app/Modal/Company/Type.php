<?php
namespace App\Modal\Company;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Modal\Company\Type;
use App\Modal\Company\Company;

class Type extends Model
{
    protected $table = 'company_types';
    protected $fillable = [
        'id',
        'logo',
        'name',
        'created_at',
        'updated_at',
    ];

    public static function get($id) { 
        try {
            return Type::where('id',$id)->with('companies')->get()[0];
          } catch (\Exception $exception) {
            return false;
          }
    }
    
      public static function change($data){
        if (isset($data['id']) &&  $data['id'] > 0) {
          $row = Type::find($data['id']); 
          foreach ($data as $key => $i) {
              if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
          }
        }else{
          unset($data['id']);
          $row = new Type($data);
        }
        $r = $row->save() ? $row : false;
        return $r;
      }
    
      public static function drop($id)
      {
          try {
            Type::find($id)->delete();
            return true;
          } catch (\Exception $exception) {
            return false;
          }
      }

    public function companies(){ return $this->belongsToMany(Company::class, 'company_to_types', 'type_id', 'company_id'); }
}