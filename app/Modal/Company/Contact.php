<?php
namespace App\Modal\Company;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use App\Modal\Company\Company;
use App\Modal\Company\Contact;

class Contact extends Model
{
  protected $table = 'company_contacts';
  protected $fillable = [
      'id',
      'created_user_id',
      'company_id',
      'contact_type',
      'contact_value',
      'status',
      'created_at',
      'updated_at',
  ];

  public static function get($id) { 
    return Contact::where('id',$id)->with('company')->get()[0];
  }

  public static function change($data){
    if (isset($data['id']) &&  $data['id'] > 0) {
      $row = Contact::find($data['id']); 
      foreach ($data as $key => $i) {
          if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
      }
    }else{
      unset($data['id']);
      $row = new Contact($data);
    }
    $r = $row->save() ? $row : false;
    return $r;
  }

  public static function drop($id)
  {
      try {
        Contact::find($id)->delete();
        return true;
      } catch (\Exception $exception) {
        return false;
      }
  }

  public function company(){ return $this->belongsTo(Company::class, 'company_id'); } 
}