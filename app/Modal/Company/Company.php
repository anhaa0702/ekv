<?php
namespace App\Modal\Company;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Modal\Company\Type;
use App\Modal\Company\Company;
use App\Modal\Company\Contact;

use App\Modal\ConstantData\CountryModal;

class Company extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $table = 'companies';

  protected $fillable = [
    'id',
    'is_main',
    'main_id',
    'status',
    'created_user_id',
    'name',
    'logo',
    'cover',
    'is_company',
    'company_rd',
    'short_desc',
    'desc',
    'city_id',
    'district_id',
    'ward_id',
    'address',
    'fax',
    'w3w',
    'long',
    'lat',
    'see_counter',
    'created_at',
    'updated_at',
    'partnership',
  ];
  
  public static function get($id) { 
    try {
        $arr = Company::where('id',$id)->with('main', 'city', 'district', 'ward')->get();
        $row = count($arr) >0 ? $arr[0] : 0;
        $row['selected_types'] = DB::table('company_to_types')->where('company_id', $id)->get()->pluck('type_id');
        return $row;
      } catch (\Exception $exception) {
        return false;
    }
  }

  public static function change($data){
    if (isset($data['id']) &&  $data['id'] > 0) {
      $row = Company::find($data['id']);
      foreach ($data as $key => $i) {
          if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
      }
    }else{
      unset($data['id']);
      $row = new Company($data);
    }
    
    if($row->save() && isset($data['selected_types'])){
      $id = $row->id;
      DB::table('company_to_types')->where('company_id', $id)->delete();
      $arr = $data['selected_types'];
      foreach ($arr as $j){
        DB::table('company_to_types')->insert(['company_id'=>$id,'type_id'   =>   $j]);
      }
    }else{
      return false;
    }
    return $row;
  }

  public static function drop($id)
  {
    try {
        $category = Company::find($id);
        $category->deleted_at = Carbon::now();
        $category->save();
        return true;
    } catch (\Exception $exception) {
        return false;
    }
  }
  
  public function city() { return $this->belongsTo(CountryModal::class, 'city_id'); }
  public function district() { return $this->belongsTo(CountryModal::class, 'district_id'); }
  public function ward() { return $this->belongsTo(CountryModal::class, 'ward_id'); }
  public function main(){ return $this->belongsTo(Company::class, 'main_id'); } 
  public function contacts(){ return $this->hasMany(Contact::class, 'company_id'); } 
  public function types(){ return $this->belongsToMany(Type::class, 'company_to_types', 'company_id', 'type_id'); }
}