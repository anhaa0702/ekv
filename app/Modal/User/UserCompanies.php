<?php
namespace App\Modal\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Modal\Loyalty\Company;
use App\Modal\User\UserCompanies;
use App\User;

class UserCompanies extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $table = 'user_to_companies';

    protected $fillable = [
        'id',
        'created_user_id',
        'company_id',
        'role',
        'status',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public static function gets($id=0){
        return UserCompanies::where('user_id', $id)->with('company', 'user','create_user')->get();
    }

    public function getsByCompany($id){
        return UserCompanies::where('company_id', $id)->get();
    }

    public static function change($data){
        // өгөгдөл шинээр үүсгэх
        if ($data['id'] == 0) {
            unset($data['id']);
            $c = new Company();
            $c->type_id = $data['type_id'];
            $c->name = $data['name'];
            $c->created_user_id = Auth::user()->id;
            $c->save();
        
            $cId = $c->id;
        
            $row = new UserCompanies();
            $row->created_user_id = Auth::user()->id;
            $row->user_id = Auth::user()->id;
            $row->company_id = $cId;
            $row->role = 'admin';
            
        } else {
            // өгөгдөл засварлах
            $row = UserCompanies::find($data['id']);

            // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
        }
        if($row->save()){
            return self::gets($row->user_id);
        }
        return false;
    }

    public function company(){ return $this->belongsTo('App\Modal\Loyalty\Company', 'company_id')->select('id', 'name'); }
    public function user(){ return $this->belongsTo(User::class, 'user_id')->select('id','firstname', 'lastname', 'avatar'); }
    public function create_user(){ return $this->belongsTo(User::class, 'created_user_id')->select('id','firstname', 'lastname', 'avatar'); }
}