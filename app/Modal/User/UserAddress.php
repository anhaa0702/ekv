<?php
namespace App\Modal\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Modal\User\UserAddress;

class UserAddress extends Model
{
  // өгөгдлийн сан дахь table
  protected $table = 'user_addresses';  
  
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  
  // талбарууд
  protected $fillable = [
    'id',
    'user_id',
    'address_type',
    'other_value',
    'city_id',
    'district_id',
    'ward_id',
    'longitude',
    'latitude',
    'address'
  ];

    // жагсаалт авах
    public static function gets($id=0){
        return UserAddress::where('user_id', $id)->with('city', 'district', 'ward')->get();
    }

    public static function change($data){
        // өгөгдөл шинээр үүсгэх
        if ($data['id'] == 0) {
            unset($data['id']);
            $row = new UserAddress($data);
        } else {
            // өгөгдөл засварлах
            $row = UserAddress::find($data['id']);

            // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
        }
        if($row->save()){
            return self::gets($row->user_id);
        }
        return false;
    }

    // relation
    public function city(){ return $this->belongsTo('App\Modal\ConstantData\CountryModal', 'city_id'); }
    public function district(){ return $this->belongsTo('App\Modal\ConstantData\CountryModal', 'district_id'); }
    public function ward(){ return $this->belongsTo('App\Modal\ConstantData\CountryModal', 'ward_id'); }

}