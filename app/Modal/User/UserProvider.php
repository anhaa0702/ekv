<?php
namespace App\Modal\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Modal\User\UserProvider;

class UserProvider extends Model
{
    
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $table = 'user_providers';
  
  // талбарууд
  protected $fillable = [
    'id',
    'user_id',
    'providers',
    'value',
    'is_active',
    'active_value',
    'created_at',
  ];

    public static function getsByUser($id=0){
        return UserProvider::where('user_id', $id)->get();
    }

    public static function change($data){
        // өгөгдөл шинээр үүсгэх
        if ($data['id'] == 0) {
            unset($data['id']);
            $row = new UserProvider($data);
        } else {
            // өгөгдөл засварлах
            $row = UserProvider::find($data['id']);

            // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
        }
        if($row->save()){
            return self::gets($row->user_id);
        }
        return false;
    }

    // жагсаалт авах
    public static function gets($id=0){
        return UserProvider::where('user_id', $id)->get();
    }
}
