<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\Modal\Content\PostFile;
use App\User;

class PostFile extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_to_file';
    protected $fillable = [
        'id',
        'access',
        'post_id',
        'uri',
        'created_at',
        'updated_at',
    ];

    

}