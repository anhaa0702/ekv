<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\Modal\Content\PostFile;
use App\Modal\Content\PostComments;
use App\Modal\Company\Company;
use App\User;

class Post extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

  protected $table = 'posts';
  protected $fillable = [
    'id',
    'post_type',
    'access',
    'is_special',
    'desc',
    'html_desc',
    'like',
    'save',
    'see',
    'company_id',
    'created_user_id',
    'created_at',
    'updated_at',
  ];

  public static function change($data){
    // өгөгдөл шинээр үүсгэх
    if (!isset($data['id']) || $data['id'] == 0) {
        unset($data['id']);
        $row = new Post($data);
        $row->created_user_id = Auth::user()->id;

    } else {
        // өгөгдөл засварлах
        $row = Post::find($data['id']);

        // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
        foreach ($data as $key => $i) {
            if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
        }
    }
    if($row->save()){
      foreach ($data['images'] as $image){
        $i = new PostFile();
        $i->access='image';
        $i->post_id=$row->id;
        $i->uri=$image['url'];
        $i->save();
      }

      foreach ($data['files'] as $image){
        $i = new PostFile();
        $i->access='file';
        $i->post_id=$row->id;
        $i->uri=$image['url'];
        $i->save();
      }
      return true;
    }
    return false;
  }

  public function comments(){ return $this->hasMany(PostComments::class, 'post_id')->join('users', 'users.id', '=', 'post_to_comments.user_id');}
  public function files(){ return $this->hasMany(PostFile::class, 'post_id'); }
  public function user(){ return $this->belongsTo(User::class, 'created_user_id')->select('id','firstname', 'lastname', 'avatar'); }
  public function company(){ return $this->belongsTo(Company::class, 'company_id')->select('id','name'); }
}