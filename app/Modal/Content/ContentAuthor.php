<?php
namespace App\Modal\Content;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Modal\Content\ContentAuthor;

class ContentAuthor extends Model
{
    protected $table = 'content_authors';
    protected $fillable = [
        'id',
        'logo',
        'full_name',
        'author_role',
        'desc',
        'created_at',
        'updated_at',
    ];

    public static function get($id) { 
        try {
            return ContentAuthor::where('id',$id)->get()[0];
          } catch (\Exception $exception) {
            return false;
          }
    }
  
    public static function change($data){
      if (isset($data['id']) &&  $data['id'] > 0) {
        $row = ContentAuthor::find($data['id']); 
        foreach ($data as $key => $i) {
            if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
        }
      }else{
        unset($data['id']);
        $row = new ContentAuthor($data);
      }
      $r = $row->save() ? $row : false;
      return $r;
    }
  
    public static function drop($id)
    {
        try {
          ContentAuthor::find($id)->delete();
          return true;
        } catch (\Exception $exception) {
          return false;
        }
    }
}