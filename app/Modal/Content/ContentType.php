<?php
namespace App\Modal\Content;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Modal\Content\Content;
use App\Modal\Content\ContentType;

class ContentType extends Model
{
    protected $table = 'content_types';
    protected $fillable = [
        'id',
        'logo',
        'name',
        'color',
        'is_special',
        'main_id',
        'pIndex',
        'mainMenu',
        'mainMenuIcon',
        'created_at',
        'updated_at',
    ];

    public static function get($id) { 
        try {
            return ContentType::where('id',$id)->get()[0];
          } catch (\Exception $exception) {
            return false;
          }
    }
  
    public static function change($data){
      if (isset($data['id']) &&  $data['id'] > 0) {
        $row = ContentType::find($data['id']); 
        foreach ($data as $key => $i) {
            if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
        }
      }else{
        unset($data['id']);
        $row = new ContentType($data);
      }
      $r = $row->save() ? $row : false;
      return $r;
    }
  
    public static function drop($id)
    {
      try {
        ContentType::find($id)->delete();
        return true;
      } catch (\Exception $exception) {
        return false;
      }
    }

    public function contents(){ return $this->belongsToMany(Content::class, 'content_to_types', 'type_id', 'content_id'); }

    public function parent() { return $this->belongsTo(ContentType::class, 'main_id'); }

    public function children() { return $this->hasMany(ContentType::class, 'main_id'); }
}