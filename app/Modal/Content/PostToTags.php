<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\User;

class PostToTags extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_to_tags';
    protected $fillable = [
        'id',
        'post_id',
        'tag_id',
        'created_at',
        'updated_at',
    ];

    

}