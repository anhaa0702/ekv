<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\User;
use App\Modal\Content\Post;
use App\Modal\Content\PostToComments;

class PostComments extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_to_comments';
    protected $fillable = [
        'id',
        'post_id',
        'comment',
        'like_count',
        'user_id',
        'created_at',
        'updated_at',
    ];

    public static function change($data){
      // өгөгдөл шинээр үүсгэх
      if (!isset($data['id']) || $data['id'] == 0) {
          unset($data['id']);
          $row = new PostComments($data);
          $row->user_id = Auth::user()->id;
      } else {
          // өгөгдөл засварлах
          $row = PostComments::find($data['id']);
  
          // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
          foreach ($data as $key => $i) {
              if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
          }
      }
      if($row->save()){
        return true;
      }
      return false;
    }

    public function user(){ return $this->belongsTo(User::class, 'user_id')->select('id','firstname', 'lastname', 'avatar'); }
    public function post(){ return $this->belongsTo(Post::class, 'post_id'); }
}