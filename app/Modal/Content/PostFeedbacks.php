<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\User;

class PostFeedbacks extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_feedbacks';
    protected $fillable = [
        'id',
        'post_id',
        'answered',
        'answer_user_id',
        'is_closed',
        'closed',
        'closed_user_id',
        'created_at',
        'updated_at',
    ];

    

}