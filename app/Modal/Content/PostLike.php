<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\Modal\Content\PostLike;

class PostLike extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_like';
    protected $fillable = [
        'id',
        'emoji',
        'post_id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    

}