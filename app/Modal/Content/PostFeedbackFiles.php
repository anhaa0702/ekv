<?php
namespace App\Modal\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Modal\Content\Post;
use App\User;

class PostFeedbackFiles extends Model
{
  // устсан төлөвт оруулах
  use SoftDeletes;
  protected $dates = ['deleted_at'];

    protected $table = 'post_feedback_files';
    protected $fillable = [
        'id',
        'access',
        'feedback_id',
        'uri',
        'created_at',
        'updated_at',
    ];

    

}