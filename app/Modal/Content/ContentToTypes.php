<?php
namespace App\Modal\Content;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Modal\Content\Content;
use App\Modal\Content\ContentType;
use App\Modal\Content\ContentToTypes;

class ContentToTypes extends Model
{
  protected $table = 'content_to_types';
  protected $fillable = [
      'id',
      'type_id',
      'content_id'
  ];
  
  public function content(){ return $this->belongsTo(Content::class, 'content_id'); }
  public function type(){ return $this->belongsToMany(ContentType::class, 'type_id'); }
}