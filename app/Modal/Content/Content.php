<?php
namespace App\Modal\Content;
use App\Services\PayUService\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Modal\Content\Content;
use App\Modal\Content\ContentType;
use App\Modal\Content\ContentAuthor;

class Content extends Model
{
    protected $table = 'contents';
    protected $fillable = [
        'id',
        'logo',
        'title',
        'short_desc',
        'desc',
        'is_active',
        'see',
        'created_at',
        'updated_at',
    ];

    public static function get($id) { 
      try {
          $arr = Content::where('id',$id)->with('types','authors')->get();
          $row = count($arr) >0 ? $arr[0] : 0;
          $row['selected_types'] = DB::table('content_to_types')->where('content_id', $id)->get()->pluck('type_id');
          $row['selected_authors'] = DB::table('content_to_authors')->where('content_id', $id)->get()->pluck('author_id');
          return $row;
      } catch (\Exception $exception) {
        return false;
      }
    }
    
    public static function change($data){
      if (isset($data['id']) &&  $data['id'] > 0) {
        $row = Content::find($data['id']);
        foreach ($data as $key => $i) {
            if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
        }
      }else{
        unset($data['id']);
        $row = new Content($data);
      }
      
      if($row->save()){
        $id = $row->id;
        DB::table('content_to_types')->where('content_id', $id)->delete();
        DB::table('content_to_authors')->where('content_id', $id)->delete();

        $arr = $data['selected_types'];
        foreach ($arr as $j){
          DB::table('content_to_types')->insert(['content_id'=>$id,'type_id'   =>   $j]);
        }

        $arr2 = $data['selected_authors'];
        foreach ($arr2 as $j){
          DB::table('content_to_authors')->insert(['content_id'=>$id,'author_id'   =>   $j]);
        }

      }else{
        return false;
      }
      return $row;
    }

    
  public static function drop($id)
  {
      try {
        DB::table('content_to_types')->where('content_id', $id)->delete();
        DB::table('content_to_authors')->where('content_id', $id)->delete();
        Content::find($id)->delete();
        return true;
      } catch (\Exception $exception) {
        return false;
      }
  }
  
  public static function getIdsByType($id=0){ return DB::table('content_to_types')->where('type_id', $id)->get()->pluck('content_id'); }
  public function authors(){ return $this->belongsToMany(ContentAuthor::class, 'content_to_authors', 'content_id', 'author_id'); }
  public function types(){ return $this->belongsToMany(ContentType::class, 'content_to_types', 'content_id', 'type_id'); }
}