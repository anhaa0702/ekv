<?php
namespace App\Modal\ConstantData;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Modal\ConstantData\BannerModal;

class BannerModal extends Model
{
    protected $table = 'banners';
    
    protected $fillable = [
        'id',
        'image',
        'title',
        'title2',
        'link',
        'is_active',
        'type',
    ];

    public static function change($data){
        // өгөгдөл шинээр үүсгэх
        if (!isset($data['id']) || $data['id'] == 0 || $data['id'] == null) {
            unset($data['id']);
            $row = new BannerModal($data);
        } else {
            // өгөгдөл засварлах
            $row = BannerModal::find($data['id']);
  
            // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
        }
        if($row->save()){
          return true;
        }
        return false;
    }

    

    public static function by_key($k){
        return BannerModal::where('type', $k)->where('is_active', 1)->get();
    }
}
