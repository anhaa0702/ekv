<?php
namespace App\Modal\ConstantData;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Modal\ConstantData\CountryModal;

class CountryModal extends Model
{
    protected $table = 'countries';

    // талбарууд
    protected $fillable = [
        'id',
        'name',
        'code',
        'country_type',
        'country_id'
    ];

    public function scopeCity()
    {
        return $this->where('country_type', 2);
    }

    public function scopeDistrict()
    {
        return $this->where('country_type', 3);
    }

    public function scopeWard()
    {
        return $this->where('country_type', 4);
    }

    public function parent()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function children()
    {
        return $this->hasMany(Country::class, 'country_id');
    }

    public function brothers()
    {
        return $this->parent->children();
    }

    public static function options($id)
    {
        if (! $self = static::find($id)) {
            return [];
        }
        return $self->brothers()->pluck('name', 'id');
    }
}
