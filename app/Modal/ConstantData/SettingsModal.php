<?php
namespace App\Modal\ConstantData;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Modal\ConstantData\SettingsModal;

class SettingsModal extends Model
{
    protected $table = 'settings';
    
    protected $fillable = [
        'id',
        'key_word',
        'image',
        'type',
        'value',
        'description'
    ];

    public static function change($data){
        // өгөгдөл шинээр үүсгэх
        if (!isset($data['id']) || $data['id'] == 0 || $data['id'] == null) {
            unset($data['id']);
            $row = new SettingsModal($data);
        } else {
            // өгөгдөл засварлах
            $row = SettingsModal::find($data['id']);
  
            // fillable утгийг шалгах - өөрчлөлт байгаа эсхээр
            foreach ($data as $key => $i) {
                if (in_array($key, $row->getFillable()) && !is_array($i)) { $row->$key = $i; }
            }
        }
        if($row->save()){
          return true;
        }
        return false;
    }

    

    public static function by_key($keys){
        // өгөгдөл шинээр үүсгэх
        if ($keys == null) {
            return SettingsModal::all();
        } else {
            return SettingsModal::whereIn('key_word', $keys)->get();
        }
        return null;
    }
}
