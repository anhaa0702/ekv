<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });

        // Schema::create('verify_users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->integer('user_id');
        //     $table->string('token');
        //     $table->timestamps();
        // });

        // Schema::create('system_default_datas', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('key_word'); // settings, ...
        //     $table->string('value_type'); // string, integer, text, boolean
        //     $table->text('value');

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_jobs');
    }
}
