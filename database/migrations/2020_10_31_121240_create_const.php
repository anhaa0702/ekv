<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateConst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->string('country_type');
            $table->bigInteger('country_id')->unsigned()->nullable()->default(null);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
        });

        Schema::create('faq', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_app')->default(true); // 1 - app, 0 - admin
            $table->integer('pIndex')->nullable()->default(null);  // харуулах дараалал
            $table->string('title')->nullable()->default(null);
            $table->text('desc')->nullable()->default(null); // html
            $table->timestamps();
        });

        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key_word');
            $table->enum('type', ['number', 'string', 'text', 'html', 'boolean', 'email', 'phone'])->default('string');
            $table->text('value')->nullable()->default(null); // html
            $table->string('description');  // ymr zoriulaltaar ashiglaj bgaa tailbar
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('faq');
        Schema::dropIfExists('settings');
    }
}
