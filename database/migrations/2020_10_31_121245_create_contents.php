<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContents extends Migration
{
    public function up()
    {
        Schema::create('post_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('post_type', ['post', 'news', 'file', 'feedback'])->default('post');
            $table->enum('access', ['public', 'main_group', 'selected_groups', 'selected_users'])->default('public');
            $table->boolean('is_special')->default(false);
            
            $table->longText('desc')->nullable()->default(null);
            $table->longText('html_desc')->nullable()->default(null);
            
            $table->integer('like')->default(0);
            $table->integer('save')->default(0);
            $table->integer('see')->default(0);
            
            $table->bigInteger('group_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('company_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('created_user_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('post_feedbacks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            
            $table->dateTime('answered')->nullable()->default(null);
            $table->bigInteger('answer_user_id')->nullable();

            $table->boolean('is_closed')->default(true);
            $table->dateTime('closed')->nullable()->default(null);
            $table->bigInteger('closed_user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    
        Schema::create('post_feedback_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('access', ['gif', 'xls', 'xlsx', 'doc', 'docx', 'odt', 'pdf', 'png', 'pptx', 'ppt', 'rtf', 'jpg', 'jpeg'])->default('png');
            $table->bigInteger('feedback_id')->unsigned();
            $table->string('uri');
            $table->foreign('feedback_id')->references('id')->on('post_feedbacks');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('post_to_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('tag_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('post_like', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('emoji', [0,1,2,3,4])->default(0);
            # 0 - like
            # 1 - hha
            # 2 - wow
            # 3 - cry
            # 4 - andry

            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('post_see', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    
        Schema::create('post_save', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    
        Schema::create('post_to_file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('access', ['gif', 'xls', 'xlsx', 'doc', 'docx', 'odt', 'pdf', 'png', 'pptx', 'ppt', 'rtf', 'jpg', 'jpeg'])->default('png');
            $table->bigInteger('post_id')->unsigned();
            $table->string('uri');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->timestamps();
            $table->softDeletes();
        });
    
        Schema::create('post_to_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned()->nullable()->default(null);
            $table->string('comment');
            $table->bigInteger('like_count');
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->timestamps();
            $table->softDeletes();
        });

        // Schema::create('images', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('image');
        //     $table->string('model_id');
        //     $table->string('type');
        //     // posts, feedbacks,feedback_comments,loyalties, teams, orgs ...
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
