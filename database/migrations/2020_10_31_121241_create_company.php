<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('company_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('logo')->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->timestamps();
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('is_main', [1,0])->default(1);
            $table->bigInteger('main_id')->unsigned()->nullable()->default(null);
            $table->enum('status', ['waiting', 'active', 'blocked'])->default('waiting');
            $table->enum('partnership', [1,0])->default(0); // нүүр хуудсан дээр харуулах байгууллагын логонууд
            $table->bigInteger('created_user_id')->unsigned()->nullable()->default(null);
            $table->foreign('type_id')->references('id')->on('company_types');
            $table->foreign('created_user_id')->references('id')->on('users');

            $table->string('name')->nullable()->default(null);
            $table->string('logo')->nullable()->default(null);
            $table->string('cover')->nullable()->default(null);

            $table->enum('is_company', [1,0])->default(1);
            
            $table->string('company_rd')->nullable()->default(null); # Регистрийн дугаар

            $table->string('short_desc')->nullable()->default(null);
            $table->longText('desc')->nullable()->default(null);

            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('district_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('ward_id')->unsigned()->nullable()->default(null);

            $table->foreign('city_id')->references('id')->on('countries');
            $table->foreign('district_id')->references('id')->on('countries');
            $table->foreign('ward_id')->references('id')->on('countries');
            $table->string('address')->nullable()->default(null);

            $table->string('fax')->nullable()->default(null);
            $table->string('w3w')->nullable()->default(null);
            $table->string('long')->nullable()->default(null);
            $table->string('lat')->nullable()->default(null);

            $table->bigInteger('see_counter')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('company_to_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_id')->nullable()->default(null);
            $table->bigInteger('company_id')->unsigned()->nullable()->default(null);
        });

        Schema::create('company_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_user_id')->nullable()->default(null);
            $table->bigInteger('company_id')->unsigned()->nullable()->default(null);
            $table->enum('contact_type', ['phone', 'email', 'web'])->default('phone');
            $table->string('contact_value')->nullable();
            $table->enum('status', ['waiting', 'active', 'blocked'])->default('waiting');
        });

        Schema::create('user_to_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_user_id')->nullable()->default(null);
            $table->bigInteger('user_id')->nullable()->default(null);
            $table->bigInteger('company_id')->unsigned()->nullable()->default(null);

            $table->enum('role', ['admin', 'moderator', 'editor'])->default('editor');
            $table->enum('status', ['waiting', 'active', 'blocked'])->default('waiting');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_types');
        Schema::dropIfExists('companies');
        Schema::dropIfExists('company_contacts');
        Schema::dropIfExists('user_to_companies');
    }
}
