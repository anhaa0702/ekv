<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoyalty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['coupon', 'voucher', 'discount'])->default('discount');
            $table->enum('status', ['all', 'members', 'members_paid', 'selected_group','selected_group_paid', 'selected_users', 'selected_users_paid'])->default('all');
            
            $table->string('image')->nullable()->default(null);
            $table->string('title');
            $table->string('desc')->nullable()->default(null);


            $table->integer('price')->default(0); // үндсэн мөнгөн дүн
            $table->integer('sale_amount')->default(0); // хөнгөлөх утга - мөнгөн дүнгээр
            $table->integer('sale_percent')->default(0); // хөнгөлөх утга
            $table->integer('use_price')->default(0);

            $table->integer('sale_count')->default(0);
            $table->integer('sold_count')->default(0);
            

            $table->bigInteger('created_org_id');
            $table->bigInteger('created_user_id');

            $table->dateTime('sale_start_at')->nullable()->default(null);
            $table->dateTime('sale_end_at')->nullable()->default(null);

            $table->dateTime('use_start_at')->nullable()->default(null);
            $table->dateTime('use_end_at')->nullable()->default(null);


            $table->bigInteger('see_counter')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

    
        Schema::create('bonus_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bonus_id')->unsigned();
            $table->string('uri');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bonus_to_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id');
            $table->bigInteger('bonus_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bonus_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_id');
            $table->bigInteger('bonus_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bonus_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('bonus_id');
            $table->string('name');
            $table->string('phone_number');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bonus_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->nullable()->default(null); # хувиаралсан
            $table->bigInteger('bonus_id')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);

            $table->bigInteger('buy_user_id')->nullable()->default(null);
            $table->bigInteger('buy_company_id')->nullable()->default(null);
            $table->dateTime('user_buy_date')->nullable()->default(null);


            $table->bigInteger('used_user_id')->nullable()->default(null);
            $table->bigInteger('used_company_id')->nullable()->default(null);
            $table->dateTime('used_date')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bonus_saved', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('bonus_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus');
        Schema::dropIfExists('bonus_to_companies');
        Schema::dropIfExists('bonus_to_groups');
        Schema::dropIfExists('bonus_to_users');
    }
}
