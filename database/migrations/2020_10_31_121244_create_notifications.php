<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('detail_id')->nullable();
            
            $table->string('object_type')->nullable();
            
            $table->string('link')->nullable();
            
            $table->string('title')->nullable();
            $table->text('message')->nullable();

            $table->dateTime('readAt')->nullable()->default(null);
            $table->dateTime('sentAt')->nullable()->default(null);
            $table->dateTime('createdAt')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
