<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('main_id')->unsigned()->nullable()->default(null);
            $table->string('name');
            $table->string('name_group')->nullable()->default(null);
            $table->string('national_number')->unique()->nullable()->default(null); # улсын бүртгэлийн дугаар
            $table->string('rd')->unique()->nullable()->default(null); # Регистрийн дугаар

            $table->longText('about')->nullable()->default(null);
            $table->string('logo')->nullable()->default(null);
            $table->string('cover')->nullable()->default(null);

            $table->enum('country_type', ['country', 'city'])->default('city');
            $table->enum('group_type', ['mueh', 'kholboo', 'sum_khoroo' ,'khoroo'])->default('khoroo');
            
            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('district_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('ward_id')->unsigned()->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('w3w')->nullable()->default(null);
            $table->string('long')->nullable()->default(null);
            $table->string('lat')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('web')->nullable()->default(null);

            $table->bigInteger('see_counter')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('groups_departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('group_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('group_socials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_id')->nullable();
            $table->string('providers')->nullable();
            $table->string('link')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->boolean('is_show_user_contact')->default(false);
            $table->timestamps();
        });

        Schema::create('group_member_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            # 1 - main
            # 2 - holboo
            # 3 - khoroo
            # 4 - sum khoroo
            $table->enum('type', [1,2,3, 4])->default(1);
            $table->enum('is_active', [1,0])->default(1);
            $table->timestamps();
        });

         DB::table('group_member_types')->insert([ 'id'=> 1,'name'=>'Ерөнхийлөгч','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 2,'name'=>'Дэд ерөнхийлөгч','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 3,'name'=>'Нарийн бичиг','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 4,'name'=>'Нягтлан','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 5,'name'=>'Хэлтэсийн ажилтан','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 6,'name'=>'Тэргүүлэх ажилтан','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 7,'name'=>'Ажилтан','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 8,'name'=>'Тэргүүлэх гишүүн','type'=>1]);
         DB::table('group_member_types')->insert([ 'id'=> 9,'name'=>'Гишүүн','type'=>2]);

         DB::table('group_member_types')->insert([ 'id'=> 101,'name'=>'Холбооны ерөнхийлөгч','type'=>2]);
         DB::table('group_member_types')->insert([ 'id'=> 102,'name'=>'Дэд дарга','type'=>2]);
         DB::table('group_member_types')->insert([ 'id'=> 103,'name'=>'Тэргүүлэх ажилтан','type'=>2]);
         DB::table('group_member_types')->insert([ 'id'=> 104,'name'=>'Ажилтан','type'=>2]);
         DB::table('group_member_types')->insert([ 'id'=> 105,'name'=>'Тэргүүлэх гишүүн','type'=>2]);
         DB::table('group_member_types')->insert([ 'id'=> 106,'name'=>'Гишүүн','type'=>2]);

         DB::table('group_member_types')->insert([ 'id'=> 1001,'name'=>'Хорооны дарга','type'=>3]);
         DB::table('group_member_types')->insert([ 'id'=> 1002,'name'=>'Орлох дарга','type'=>3]);
         DB::table('group_member_types')->insert([ 'id'=> 1003,'name'=>'Тэргүүлэх ажилтан','type'=>3]);
         DB::table('group_member_types')->insert([ 'id'=> 1004,'name'=>'Ажилтан','type'=>3]);
         DB::table('group_member_types')->insert([ 'id'=> 1005,'name'=>'Тэргүүлэх гишүүн','type'=>3]);
         DB::table('group_member_types')->insert([ 'id'=> 1006,'name'=>'Гишүүн','type'=>3]);

         DB::table('group_member_types')->insert([ 'id'=> 2001,'name'=>'Хорооны дарга','type'=>4]);
         DB::table('group_member_types')->insert([ 'id'=> 2002,'name'=>'Орлох дарга','type'=>4]);
         DB::table('group_member_types')->insert([ 'id'=> 2003,'name'=>'Тэргүүлэх ажилтан','type'=>4]);
         DB::table('group_member_types')->insert([ 'id'=> 2004,'name'=>'Ажилтан','type'=>4]);
         DB::table('group_member_types')->insert([ 'id'=> 2005,'name'=>'Тэргүүлэх гишүүн','type'=>4]);
         DB::table('group_member_types')->insert([ 'id'=> 2006,'name'=>'Гишүүн','type'=>4]);

        Schema::create('user_to_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->default(null);
            
            $table->enum('group_type', ['mueh', 'kholboo', 'sum_khoroo' ,'khoroo'])->default('khoroo');
            $table->bigInteger('group_id')->nullable()->default(null);


            $table->enum('access_type', ['unknown','admin', 'moderator', 'editor', 'user'])->default('unknown');
            $table->enum('access', ['waiting', 'approved', 'blocked'])->default('waiting');

            $table->dateTime('approved_at')->nullable()->default(null);
            $table->bigInteger('approved_user_id')->nullable()->default(null);

            $table->boolean('is_main')->default(false);
            $table->date('paid_main_start_at')->nullable();
            $table->date('paid_main_end_at')->nullable();
            $table->boolean('is_print_batlah')->default(false);

            $table->boolean('is_paid')->default(false);
            $table->boolean('is_lock')->default(false);
            $table->dateTime('locked_at')->nullable()->default(null);
            $table->bigInteger('locked_user_id')->nullable()->default(null);
            $table->bigInteger('user_type_id')->nullable()->default(null);
            $table->text('notes')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_to_group_departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_id')->nullable()->default(null);
            $table->bigInteger('user_id')->nullable()->default(null);
            $table->timestamps();
        });

        Schema::create('user_to_group_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_id')->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_socials');
        Schema::dropIfExists('user_to_groups');
    }
}
