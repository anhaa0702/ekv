<?php
use Illuminate\Database\Seeder;
use App\OrgType;
use App\Org;
use App\User;
class OrgTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $otype1=OrgType::create([ "title"=> 'Их сургууль']);
        $otype2=OrgType::create([ "title"=> 'Хүнсний дэлгүүр']);
        $otype3=OrgType::create([ "title"=> 'Барааны дэлгүүр']);
        $otype4=OrgType::create([ "title"=> 'Сүлжээ дэлгүүр']);
        $otype5=OrgType::create([ "title"=> 'Ресторан']);
        $otype6=OrgType::create([ "title"=> 'Зоогын газар']);
        $otype7=OrgType::create([ "title"=> 'Зочид буудал']);
        $otype8=OrgType::create([ "title"=> 'Үсчин']);
        $otype9=OrgType::create([ "title"=> 'Гоо сайхны салон']);
        $otype10=OrgType::create([ "title"=> 'Авто сэлбэг']);
        $otype11=OrgType::create([ "title"=> 'Авто засвар']);
        $otype12=OrgType::create([ "title"=> 'Авто машин худалдаа']);
        $otype13=OrgType::create([ "title"=> 'Гоо сайхны бараа']);
        $otype14=OrgType::create([ "title"=> 'Цахилгаан бараа']);
        $otype15=OrgType::create([ "title"=> 'Номын худалдаа']);
        $otype16=OrgType::create([ "title"=> 'Бэлэг дурсгал']);
        $otype17=OrgType::create([ "title"=> 'Хүнсний бүтээгдэхүүн']);
        $otype18=OrgType::create([ "title"=> 'Мах махан бүтээгдэхүүн']);
        $otype19=OrgType::create([ "title"=> 'Сүү, сүүн бүтээгдэхүүн']);
        $otype20=OrgType::create([ "title"=> 'Эмнэлэг']);
        $otype21=OrgType::create([ "title"=> 'Амралт']);
        $otype22=OrgType::create([ "title"=> 'Сувилал']);
        $otype23=OrgType::create([ "title"=> 'Мэргэжлийн сургалтын төв']);
        $otype24=OrgType::create([ "title"=> 'Гадаад хэлний сургалтын төв']);
        $otype25=OrgType::create([ "title"=> 'Орчуулгын товчоо']);
        $otype26=OrgType::create([ "title"=> 'Барилга']);
        $otype27=OrgType::create([ "title"=> 'Орон сууц']);
        $otype28=OrgType::create([ "title"=> 'Цэцэрлэг']);
        $otype29=OrgType::create([ "title"=> 'Бага сургууль']);
        $otype30=OrgType::create([ "title"=> 'Дунд сургууль']);
        $otype31=OrgType::create([ "title"=> 'Анхлах сургууль']);
        $otype32=OrgType::create([ "title"=> 'Үзвэр үйлчилгэний байгууллага']);

        
        $user1001=User::create(['password'=> bcrypt('123123'), 'lastname'=> 'Г','firstname'=>'Баярмаа','login_name'=>'g.bayarmaa','phone'=> '94004758']);
        $user1002=User::create(['password'=> bcrypt('123123'), 'lastname'=> 'Н','firstname'=>'Энхманлай','login_name'=>'n.enkhmaa','phone'=> '99079045']);
        $user1003=User::create(['password'=> bcrypt('123123'), 'lastname'=> 'Ц','firstname'=>'Мөнхбаяр','login_name'=>'ts.monkhbayar','phone'=> '99180750']);
        $user1004=User::create(['password'=> bcrypt('123123'), 'lastname'=> 'Ж','firstname'=>'Эрхэмбаяр','login_name'=>'j.erkhembayar','phone'=> '88118810']);

        Org::create([ "name"=> 'Хөдөлмөр, нийгмийн харилцааны дээд сургууль', 'phone'=>'11312629','user_id'=>$user1001['id'],'org_type_id'=>$otype1['id']]);
        Org::create([ "name"=> '“Альфа” зочид буудал', 'phone'=>'75758833','user_id'=>$user1002['id'],'org_type_id'=>$otype7['id']]);
        Org::create([ "name"=> 'МҮЭ-ийн Соёлын төв ордон', 'phone'=>'70119603','user_id'=>$user1003['id'],'org_type_id'=>$otype32['id']]);
        Org::create([ "name"=> 'Үйлдвэрлэл үйлчилгээний төв', 'phone'=>'11315481','org_type_id'=>$otype32['id']]);
        Org::create([ "name"=> '“Сонгино” амралт', 'phone'=>'70492121','user_id'=>$user1004['id'],'org_type_id'=>$otype21['id']]);
    }
}
