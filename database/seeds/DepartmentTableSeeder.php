<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Department;
use App\Content;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99879858','user_type_id'=>'101','team_id'=> 2, 'lastname'=> 'Б','firstname'=>'Цэрэнпүрэв','login_name'=>'b.tserenpurev','email'=>'tserenpurev2008@yahoo.com','work_phone'=>'328190']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '89036109','user_type_id'=>'103','team_id'=> 2, 'lastname'=> 'М','firstname'=>'Одончимэг','login_name'=>'m.odonchineg']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99142999','user_type_id'=>'101','team_id'=> 3, 'lastname'=> 'М','firstname'=>'Алтанцэцэг','login_name'=>'m.altantsetseg','email'=>'altan_m2001@yahoo.com','work_phone'=>'321964']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '94003004','user_type_id'=>'103','team_id'=> 3, 'lastname'=> 'М','firstname'=>'Ганхүрэл','login_name'=>'m.ganhurel','work_phone'=>'96659427']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '88276978','user_type_id'=>'103','team_id'=> 3, 'lastname'=> 'Д','firstname'=>'Өлзийсайхан','login_name'=>'d.ulziisaihan','work_phone'=>'99054447']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '96884647','user_type_id'=>'101','team_id'=> 4, 'lastname'=> 'Б','firstname'=>'Батцогт','login_name'=>'b.battsogt','email'=>'tsogoo011064@yahoo.com','work_phone'=>'70127147']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99618357','user_type_id'=>'103','team_id'=> 4, 'lastname'=> 'Ц','firstname'=>'Энхцэцэг','login_name'=>'ts.enhtsetseg']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99520799','user_type_id'=>'103','team_id'=> 4, 'lastname'=> 'Н','firstname'=>'Таня','login_name'=>'n.tanya']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99802050','user_type_id'=>'101','team_id'=> 5, 'lastname'=> 'Ж','firstname'=>'Батзориг','login_name'=>'j.batzorig','email'=>'zorigjak@yahoo.com','work_phone'=>'323555']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99030510','user_type_id'=>'103','team_id'=> 5, 'lastname'=> 'Д','firstname'=>'Цэцэгдэлгэр','login_name'=>'d.tsetsegdelger']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99186592','user_type_id'=>'103','team_id'=> 5, 'lastname'=> 'Г','firstname'=>'Цэцэгмаа','login_name'=>'g.tsetsegmaa']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '93153431','user_type_id'=>'103','team_id'=> 5, 'lastname'=> 'З','firstname'=>'Цогтгэрэл','login_name'=>'z.tsoggerel']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '91180312','user_type_id'=>'103','team_id'=> 5, 'lastname'=> 'Б','firstname'=>'Бат-Энх','login_name'=>'b.batenhk']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99136258','user_type_id'=>'101','team_id'=> 6, 'lastname'=> 'Б','firstname'=>'Батчулуун','login_name'=>'b.batchuluun','email'=>'batchuluun.batsukh@yahoo.com','work_phone'=>'322554']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '91189996','user_type_id'=>'103','team_id'=> 6, 'lastname'=> 'С','firstname'=>'Дэмбэрэл','login_name'=>'s.demberel','work_phone'=>'']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99086972','user_type_id'=>'101','team_id'=> 7, 'lastname'=> 'Г','firstname'=>'Дүгэрээ','login_name'=>'g.dugeree','work_phone'=>'326459']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99066473','user_type_id'=>'103','team_id'=> 7, 'lastname'=> 'Ж','firstname'=>'Оюунсайхан','login_name'=>'j.oyunsaikhan']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99704379','user_type_id'=>'101','team_id'=> 8, 'lastname'=> 'Я','firstname'=>'Алтантуяа','login_name'=>'y.altantuya','email'=>'yaamts@yahoo.com','work_phone'=>'323056']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99242236','user_type_id'=>'103','team_id'=> 8, 'lastname'=> 'Ж','firstname'=>'Долгорсүрэн','login_name'=>'j.dolgorsuren']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99260951','user_type_id'=>'103','team_id'=> 8, 'lastname'=> 'Г','firstname'=>'Баасанцэрэн','login_name'=>'g.baasantseren']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99909199','user_type_id'=>'101','team_id'=> 9, 'lastname'=> 'Х','firstname'=>'Эрдэнэ','login_name'=>'kh.erdene','work_phone'=>'21-244000']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99295476','user_type_id'=>'102','team_id'=> 9, 'lastname'=> 'н','firstname'=>'Батчулуун','login_name'=>'batchuluun']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99992805','user_type_id'=>'103','team_id'=> 9, 'lastname'=> 'Д','firstname'=>'Нарантуяа','login_name'=>'d.narantuya','work_phone'=>'244002']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '96651164','user_type_id'=>'103','team_id'=> 9, 'lastname'=> 'Б','firstname'=>'Алтанхуяг','login_name'=>'n.altanhuyag','work_phone'=>'244003']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '96654068','user_type_id'=>'103','team_id'=> 9, 'lastname'=> 'Ө','firstname'=>'Оюунчимэг','login_name'=>'u.oyubchimeg','work_phone'=>'244005']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '86112103','user_type_id'=>'103','team_id'=> 9, 'lastname'=> 'Б','firstname'=>'Энхтөгс','login_name'=>'b.enkhtugs','work_phone'=>'244003']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99041540','user_type_id'=>'103','team_id'=> 9, 'lastname'=> 'Н','firstname'=>'Отгонбаяр','login_name'=>'n.otgonbayr','work_phone'=>'244004']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '88001733','user_type_id'=>'101','team_id'=> 37, 'lastname'=> 'Б','firstname'=>'Рагчаа','login_name'=>'b.ragchaa','email'=>'ragchaa_bayaraa@yahoo.com','work_phone'=>'327955']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '86037878','user_type_id'=>'103','team_id'=> 37, 'lastname'=> 'М','firstname'=>'Болдсайхан','login_name'=>'m.boldsaikhan']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99905733','user_type_id'=>'103','team_id'=> 37, 'lastname'=> 'С','firstname'=>'Золбаяр','login_name'=>'s.zolbayr']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '91034888','user_type_id'=>'103','team_id'=> 37, 'lastname'=> 'Д','firstname'=>'Эрдэнэцоо','login_name'=>'d.erdenetsoo']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99768881','user_type_id'=>'101','team_id'=> 37, 'lastname'=> 'Ч','firstname'=>'Чимэг','login_name'=>'ch.chimeg','work_phone'=>'311319']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '88789379','user_type_id'=>'103','team_id'=> 37, 'lastname'=> 'Б','firstname'=>'Отгонцэцэг','login_name'=>'b.otgontsetseg','work_phone'=>'']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99059153','user_type_id'=>'101','team_id'=> 11, 'lastname'=> 'О','firstname'=>'Гантуяа','login_name'=>'o.gantuya','email'=>'gantuyaafe@yahoo.com','work_phone'=>'323658']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99793482','user_type_id'=>'103','team_id'=> 11, 'lastname'=> 'Д','firstname'=>'Уранчимэг','login_name'=>'d.uranchimeg']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '91992213','user_type_id'=>'101','team_id'=> 12, 'lastname'=> 'Б','firstname'=>'Мягмар','login_name'=>'b.mygmar','email'=>'mmetuf@yahoo.com','work_phone'=>'323037']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','user_type_id'=>'102','team_id'=> 12, 'lastname'=> 'Ч','firstname'=>'Нарантуяа','login_name'=>'ch.narantuya','work_phone'=>'320549']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','user_type_id'=>'103','team_id'=> 12, 'lastname'=> 'Л','firstname'=>'Батчимэг','login_name'=>'l.batchimeg','work_phone'=>'320549']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','user_type_id'=>'103','team_id'=> 12, 'lastname'=> 'Т','firstname'=>'Норжинсүрэн','login_name'=>'t.norjinsuren','work_phone'=>'320549']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99351145','user_type_id'=>'101','team_id'=> 13, 'lastname'=> 'Н','firstname'=>'Батбаатар','login_name'=>'n.batbaatar','email'=>'erdenetue@yahoo.com','work_phone'=>'']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99093460','user_type_id'=>'101','team_id'=> 14, 'lastname'=> 'Х','firstname'=>'Буянжаргал','login_name'=>'h.buyanjargal','email'=>'buya_ph69@yahoo.com','work_phone'=>'322028']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99907026','user_type_id'=>'102','team_id'=> 14, 'lastname'=> 'Д','firstname'=>'Долгор','login_name'=>'d.dolgor']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '88837670','user_type_id'=>'103','team_id'=> 14, 'lastname'=> 'Д','firstname'=>'Болд','login_name'=>'d.bold']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99178729','user_type_id'=>'103','team_id'=> 14, 'lastname'=> 'О','firstname'=>'Баярхүү','login_name'=>'o.bayarkhuu']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '94565628','user_type_id'=>'101','team_id'=> 15, 'lastname'=> 'Л','firstname'=>'Энхтайван','login_name'=>'l.enkhtaiwan','email'=>'l_enkh33@yahoo.com','work_phone'=>'70332702']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '98226116','user_type_id'=>'101','team_id'=> 16, 'lastname'=> 'А','firstname'=>'Кеншилик','login_name'=>'a.kenshilik','email'=>'buueh@yahoo.com','work_phone'=>'70422731']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '89528882','user_type_id'=>'101','team_id'=> 17, 'lastname'=> 'В','firstname'=>'Гүрсэд','login_name'=>'w.gursed','email'=>'v_gursed@yahoo.com','work_phone'=>'']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '94455005','user_type_id'=>'101','team_id'=> 18, 'lastname'=> 'Д','firstname'=>'Баярсайхан','login_name'=>'d.bayarsaikhan','email'=>'bayrasan1024@gmail.com','work_phone'=>'70342990']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99024871','user_type_id'=>'101','team_id'=> 19, 'lastname'=> 'Л','firstname'=>'Сумаахүү','login_name'=>'l.sumhuu','email'=>'summa31@yahoo.com','work_phone'=>'70483499']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99897544','user_type_id'=>'101','team_id'=> 20, 'lastname'=> 'Д','firstname'=>'Гантөмөр','login_name'=>'d.gantumur']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99378626','user_type_id'=>'101','team_id'=> 21, 'lastname'=> 'Д','firstname'=>'Нацагдорж','login_name'=>'d.natsagdorj','email'=>'d.natsagdorj17@yahoo.com','work_phone'=>'70377040']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99005844','user_type_id'=>'101','team_id'=> 30, 'lastname'=> 'С','firstname'=>'Азжаргал','login_name'=>'s.azjargal','email'=>'azjargal_tu@yahoo.com','work_phone'=>'70522333']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99065073','user_type_id'=>'101','team_id'=> 31, 'lastname'=> 'Л','firstname'=>'Цагаан','login_name'=>'l.tsagaan','email'=>'tsag2212@yahoo.com','work_phone'=>'70584347']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '95941958','user_type_id'=>'101','team_id'=> 32, 'lastname'=> 'Б','firstname'=>'Мөнхчулуун','login_name'=>'b.munhchuluun','email'=>'gmn_2009@yahoo.com']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '98600820','user_type_id'=>'101','team_id'=> 33, 'lastname'=> 'Б','firstname'=>'Ганбаатар','login_name'=>'b.ganbaatar','email'=>'zavkhan.print@gmail.com','work_phone'=>'70462214']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '94261008','user_type_id'=>'101','team_id'=> 34, 'lastname'=> 'М','firstname'=>'Батцоож','login_name'=>'m.battsooj','email'=>'battsooj_tu@yahoo.com','work_phone'=>'70359412']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99329435','user_type_id'=>'101','team_id'=> 35, 'lastname'=> 'Ц','firstname'=>'Баярбат','login_name'=>'ts.bayrbat','email'=>'ftu.uvbayarbat@yahoo.com','work_phone'=>'70322722']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99207787','user_type_id'=>'101','team_id'=> 36, 'lastname'=> 'Д','firstname'=>'Мөнх-Эрдэнэ','login_name'=>'d.munkherdene','email'=>'moogii_tg@yahoo.com','work_phone'=>'70533640']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99009501','user_type_id'=>'101','team_id'=> 22, 'lastname'=> 'О','firstname'=>'Батзаяа','login_name'=>'o.batzaya','email'=>'one_99009501@yahoo.com','work_phone'=>'70518428']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99248649','user_type_id'=>'101','team_id'=> 23, 'lastname'=> 'Ж','firstname'=>'Саранцэцэг','login_name'=>'j.sarantsetseg','email'=>'j.saran955@yahoo.com']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99309991','user_type_id'=>'101','team_id'=> 24, 'lastname'=> 'Д','firstname'=>'Оюун','login_name'=>'dorjpalam.oyun','email'=>'dorjpalam.oyun@yahoo.com','work_phone'=>'70273227']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99456796','user_type_id'=>'101','team_id'=> 25, 'lastname'=> 'Ц','firstname'=>'Лумжав','login_name'=>'ts.lumjaw','email'=>'lumjav_hd@yahoo.com','work_phone'=>'70453518']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99501757','user_type_id'=>'101','team_id'=> 26, 'lastname'=> 'Ц','firstname'=>'Ариунжаргал','login_name'=>'ts.ariunjargal','email'=>'bebe_abt@yahoo.com','work_phone'=>'70433905']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '88507359','user_type_id'=>'101','team_id'=> 27, 'lastname'=> 'Ц','firstname'=>'Ганболд','login_name'=>'ts.ganbold','email'=>'ganbold5500@yahoo.com','work_phone'=>'70382611']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '95958890','user_type_id'=>'101','team_id'=> 28, 'lastname'=> 'Д','firstname'=>'Оюун','login_name'=>'d.oyun','email'=>'doyun8890@yahoo.com','work_phone'=>'70563643']);
        User::create(['password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','phone'=> '99197184','user_type_id'=>'101','team_id'=> 29, 'lastname'=> 'С','firstname'=>'Жаргалсайхан','login_name'=>'s.jargalsaikhan','email'=>'jagaat@yahoo.com','work_phone'=>'320313']);

        $d=Department::create([ "name"=> '“Хөдөлмөр” сургалт, судалгааны төв','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99122351', 'lastname'=> 'М','firstname'=>'Ганаа','login_name'=>'m.ganaa','work_phone'=>'70120299']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '95950518', 'lastname'=> 'Б','firstname'=>'Насанжаргал','login_name'=>'b.nasanjargal','work_phone'=>'70120412']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99739410', 'lastname'=> 'М','firstname'=>'Анударь','login_name'=>'m.anudari','work_phone'=>'70120412']);

        $d=Department::create(['name'=>'Гадаад харилцаа, олон нийттэй харилцах хэлтэс','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99196133', 'lastname'=> 'Э','firstname'=>'Амарсанаа','login_name'=>'m.amarsanaa','work_phone'=>'70111598']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99620828', 'lastname'=> 'Ц','firstname'=>'Намуун','login_name'=>'m.namuun','work_phone'=>'70111598']);

        $d=Department::create(['name'=>'Санхүү, аж ахуйн газар','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99105034', 'lastname'=> 'Э','firstname'=>'Баатарцогт','login_name'=>'e.baatartsogt','work_phone'=>'70126934']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88072686', 'lastname'=> 'Н','firstname'=>'Мөнхтуул','login_name'=>'n.monkhtuul','work_phone'=>'70126934']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '89149626', 'lastname'=> 'Ч','firstname'=>'Мядагмаа','login_name'=>'ch.mydagmaa','work_phone'=>'70126934']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99015175', 'lastname'=> 'А','firstname'=>'Жаргалсайхан','login_name'=>'a.jargalmaa','work_phone'=>'70126934']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88096310', 'lastname'=> 'Г','firstname'=>'Наранжаргал','login_name'=>'g.naranjargal','work_phone'=>'70126934']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88039841', 'lastname'=> 'М','firstname'=>'Урантулга','login_name'=>'m.urantulga','work_phone'=>'96024587']);

        $d=Department::create(['name'=>'Хэвлэл мэдээлэл, сурталчилгааны алба','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99866128', 'lastname'=> 'С','firstname'=>'Оюунтөгс','login_name'=>'c.oyuntogos']);
        $u= User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88007709', 'lastname'=> 'Б','firstname'=>'Хосбаяр','login_name'=>'b.khosbayar']);
        
        $c=Content::create(['is_public'=>true, 'user_id'=>$u['id'], 'is_show_follew_team'=>false, 'desc'=> 'Нэг зөрүүд авгай бүхэл бүтэн Хангүг улсыг самарч байхад Нэг хэсүүлч хамаатан чинь танай удмыг сүйрүүлж мэднэ шүү. Цахимаар золгоцгооё !!']);
        DB::table('content_to_images')->insert(['content_id'=>$c['id'], 'uri'=>'https://img2.yna.co.kr/etc/graphic/YH/2020/02/24/GYH2020022400100031500_P4.jpg']);


        $u= User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99027019', 'lastname'=> 'О','firstname'=>'Булгантамир','login_name'=>'o.bulgantamir']);

        $c=Content::create(['is_public'=>true, 'user_id'=>$u['id'], 'is_show_follew_team'=>false, 'desc'=> 'Яагаад 30хан настай залуухан ээж шалтгаангүйгээр хорвоог орхих ёстой гэж???
        Яагаад 30 хан настай Төрийн албан хаагч ид ажиллах насан дээрээ учир битүүлгээр амиа алдах гэж???
        Яагаад 30 хан настай залуу Эмэгтэй ид хийж бүтээх насандаа хорвоогийн мөнх бусыг үзэх гэж
        Яагаад??? Яагаад??? Яагаад???
        5н настай хүүхдийн тавих ёстой асуултыг бид нэгнээсээ асуугаад үнэнийг эрэлхийлэх ёстой гэж?
        Хаана бна тэр ХҮН ЧАНАР? ХҮНИЙ ЭРХ?
        Хайран хүний охин, хичнээн сайн найз...
        Дарга нэрэндээ дулдуйдсан далдайсан хар новшнуудыг...Зайлуулаад өгөөч тэр, зайгуул зуугуул сэтгэлтэй, заналт шуналт сүгнүүдийг.../гадны нөлөөтэй/']);
        DB::table('content_to_images')->insert(['content_id'=>$c['id'], 'uri'=>'https://files.consumerfinance.gov/f/images/201702_cfpb_Oah_Blog6-credit_score.original.png']);
        $c=Content::create(['is_public'=>true, 'user_id'=>$u['id'], 'is_show_follew_team'=>false, 'desc'=> 'ҮЭ ийн ахмад зүтгэлтэн Эрдэнэбат гуай Үйлчилгээний гавъяат ажилтан болжээ. Баяр хүргэж, эрүүл энх, сайн сайхан бүхнийг хүсьe.']);
        

        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '89024041', 'lastname'=> 'Т','firstname'=>'Солонго','login_name'=>'t.solongo']);

        $d=Department::create(['name'=>'ҮЭ-ийн хөдөлмөрийн хяналт, хуулийн бодлогын газар','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99016953', 'lastname'=> 'Ц','firstname'=>'Отгонтунгалаг','login_name'=>'ts.otgontungalag','work_phone'=>'70127440']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99667610', 'lastname'=> 'С','firstname'=>'Эрдэнэбат','login_name'=>'s.erdenebat','work_phone'=>'70127440']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88078357', 'lastname'=> 'М','firstname'=>'Саранцацрал','login_name'=>'s.sarantsatsral','work_phone'=>'70119989']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99743717', 'lastname'=> 'М','firstname'=>'Одбаяр','login_name'=>'s.odbayar','work_phone'=>'1800-1292']);


        $d=Department::create(['name'=>'ҮЭ-ийн Хөдөлмөр, нийгмийн хамгааллын бодлого төлөвлөлтийн газар','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99013768', 'lastname'=> 'М','firstname'=>'Нямдаваа','login_name'=>'m.nymdavaa','work_phone'=>'70127440']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '89047990', 'lastname'=> 'М','firstname'=>'Баярмаа','login_name'=>'m.bayarmaa','work_phone'=>'70127440']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99705637', 'lastname'=> 'Ц','firstname'=>'Халиунаа','login_name'=>'ts.khaliunaa','work_phone'=>'70127440']);

        $d=Department::create(['name'=>'ҮЭ-ийн бодлого зохион байгуулалтын газар','team_id'=>1]);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88001248', 'lastname'=> 'Г','firstname'=>'Төмөрбат','login_name'=>'g.tumurbat','work_phone'=>'70125857']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88745555', 'lastname'=> 'Л','firstname'=>'Цэрмаа','login_name'=>'l.tsermaa','work_phone'=>'70125857']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '88062101', 'lastname'=> 'Э','firstname'=>'Тамир','login_name'=>'e.tamir','work_phone'=>'70125857']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '99842759', 'lastname'=> 'Ц','firstname'=>'Сэлэнгэ','login_name'=>'ts.selenge','work_phone'=>'70112128']);
        User::create(['department_id'=>$d['id'], 'user_type_id'=>5,'password'=> bcrypt('123123'), 'team_verified'=>'2020-02-25 14:39:45','team_id'=> 1,'phone'=> '89892002', 'lastname'=> 'Б','firstname'=>'Оюунзул','login_name'=>'b.oyunzul','work_phone'=>'70113292']);
    }
}
