<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $co1 = Team::create(['name'=>'Монголын Үйлдвэрчний Эвлэлийн Холбоо','name_short'=>'EKV','team_type'=>1]);
        $co2 = Team::create(['name'=>'Авто замчдын ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co3 = Team::create(['name'=>'Ажилтан албан хаагчдын ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co4 = Team::create(['name'=>'Барилгачдын ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co5 = Team::create(['name'=>'Боловсрол, шинжлэх ухааны ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co6 = Team::create(['name'=>'Үйлдвэрлэл ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co7 = Team::create(['name'=>'Жижиг дунд үйлдвэр, худалдаа ахуй үйлчилгээ, аялал жуулчлалын салбарын ажилтны ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co8 = Team::create(['name'=>'Нэгдмэл ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co9 = Team::create(['name'=>'Төмөр замчдын ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        
        $co11 = Team::create(['name'=>'Хот байгуулалт, нийтийн аж ахуйн ажилтны ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co12 = Team::create(['name'=>'Хүнс, хөдөө аж ахуй, байгаль орчны ажилтны ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co13 = Team::create(['name'=>'Эрүүл мэндийн ажилтны ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co14 = Team::create(['name'=>'“Эрдэнэт үйлдвэр” ХХК-ийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co15 = Team::create(['name'=>'Эрчим хүч, геологи уул уурхайн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        $co16 = Team::create(['name'=>'Архангай аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co17 = Team::create(['name'=>'Баян-Өлгий аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co18 = Team::create(['name'=>'Баянхонгор аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co19 = Team::create(['name'=>'Булган аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co20 = Team::create(['name'=>'Говь-Алтай аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co21 = Team::create(['name'=>'Говьсүмбэр аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co22 = Team::create(['name'=>'Дархан-Уул аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co23 = Team::create(['name'=>'Сүхбаатар аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co24 = Team::create(['name'=>'Сэлэнгэ аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co25 = Team::create(['name'=>'Төв аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co26 = Team::create(['name'=>'Увс аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co27 = Team::create(['name'=>'Ховд аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co38 = Team::create(['name'=>'Хөвсгөл аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co39 = Team::create(['name'=>'Хэнтий аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co30 = Team::create(['name'=>'Нийслэл Улаанбаатар хотын ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $t = Team::create(['name'=>'Дорноговь аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co32 = Team::create(['name'=>'Дорнод аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co33 = Team::create(['name'=>'Дундговь аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co34 = Team::create(['name'=>'Завхан аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co35 = Team::create(['name'=>'Орхон аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co36 = Team::create(['name'=>'Өвөрхангай аймгийн ҮЭ-ийн холбоо','team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        $co37 = Team::create(['name'=>'Өмнөговь аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);

        // ----------------------------- Тээвэр холбоо, газрын тосны ажилтны ҮЭ-ийн холбоо ----------------------------- //
        $t = Team::create(['name'=>'Тээвэр холбоо, газрын тосны ажилтны ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>false ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'МИАТ ТӨХК-ний ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Иргэний нисэхийн ерөнхий газрын ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н харьяа холбоо навигаци, ажиглалтын үйлчилгээний албаны ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н харьяа нислэгийн хөдөлгөөний үйлчилгээний албаны ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н харьяа газрын тусгай үйлчилгээний албаны ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н харьяа нисэхийн мэдээллийн үйлчилгээний албаны ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н харьяа шалгалтын нислэг зураглалын албаны ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Чингис хаан Олон улсын нисэх буудлын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'ИНЕГ-н нислэг цаг уурын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монголын цахилгаан холбоо ХК-ний маркетинг борлуулалт үйлчилгээний газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монголын цахилгаан холбоо ХК-ний технологийн газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монголын цахилгаан холбоо ХК-ний захиргаа удирдлагын газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монголын цахилгаан холбоо ХК-ний ҮЭ-ийн нэгдсэн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монголын цахилгаан холбоо ХК-ний Төв аймгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Мэдээлэл холбоо сүлжээ ХХК-ний УБ хот дахь салбарын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Мэдээлэл холбоо сүлжээ ХХК-ний үндсэн сүлжээний газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Мэдээлэл холбоо сүлжээ ХХК-ний аж ахуйн үйлчилгээний хэлтсийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Мэдээлэл холбоо сүлжээ ХХК-ний ҮЭ-ийн нэгдсэн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монгол шуудан ХК-ний ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Радио телевизийн үндэсний сүлжээ улсын төсөвт үйлдвэрийн газрын ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Зорчигч тээврийн нэгтгэл ОНӨААТҮГ-ын автобус 1 дүгээр баазын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Зорчигч тээврийн нэгтгэл ОНӨААТҮГ-ын автобус 2 дугаар баазын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Зорчигч тээвэр 3 ОНӨААТҮГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Блюбус ХХК-ний ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Нийслэлийн нийтийн тээврийн газрын ажилтны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Авто тээврийн үндэсний төв ТӨШГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Хувиараа тээвэр эрхлэгчдийн ҮЭ-ийн нэгдсэн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Дорны саруул зам хоршооны ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Петростар ХХК-ний ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Петровис трейдинг ХХК-ний ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Петрочайна дачин тамсаг ХХК-ний ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Говь транс ложистикс ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'МИАТ-ийн нисгэгчдийн эрх ашгийг хамгаалах ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Эрдэмтранс ХХК-ийн ҮЭ-ний хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Тэнүүн-Огоо ХХК-ний ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Таван толгой шин ХХК-ийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Монпорт ХХК-ний ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  false, 'name'=>  'Дорны саруул зам ҮЭ-ийн хороо' ]);

        // ----------------------------- Дорноговь аймгийн ҮЭ-ийн холбоо ----------------------------- //
        $t = Team::create(['name'=>'Дорноговь аймгийн ҮЭ-ийн холбоо', 'team_id'=> $co1['id'],'team_type'=>2,'is_country'=>true]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Айраг сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Алтанширээ сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Даланжаргалан сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дэлгэрэх сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Иххэт сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Мандах сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Өргөн сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайхандулаан сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Улаанбадрах сумын МҮЭ хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Хатанбулаг сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Хөвсгөл сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Эрдэнэ сумын МҮЭ холбоо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Анагаах ухааны коллежийн МҮЭ хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Мэргэжлийн сургалт үйлдвэрлэлийн төвийн ажиллагсадын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Төмөр замын мэргэжлийн сургалт үйлдвэрлэлийн төвийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 1-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 2-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 3-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд 1-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд 2-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд 3-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Зүүнбаян 4-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 3-р Цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 4-р Цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Аймгийн сувилалын цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 8-р Цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 11-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 12-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 13-р Цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд 5-р багийн цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд сумын хүүхдийн цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд сумын 3-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд 6-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Саран хөхөө театрын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Аймгийн соёлын байгууллагын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Биеийн тамир спортын газрын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Номын сангийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Хөдөлмөр халамж үйлчилгээний хэлтэс ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Хүүхдийн төлөө төвийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Зэвсэгт хүчний 336-р ангийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Аймгийн үсчин гоо засалчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Нийтийн ахуй үйлчилгээний Говийн баялаг бүтээгчид ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Тохижилт сайншанд хот тохижилтийн газрын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд хот тохижуулалтын албаны ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Нийтлэг үйлчилгээний газрын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Нийгмийн даатгалын хэлтсийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Хүнс хөдөө аж ахуйн ЖДҮ-ийн газрын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Нисэх буудлын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Монголын цахилгаан холбоо Дорноговь салбар ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дорноговь аймгийн Мэдээлэл холбоо сүлжээ ТӨХК ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дорноговь Эрчим хүчний нэгдсэн үйлдвэрийн EKVороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Чандмань бадрал ХХК ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Доншен газрын тос монгол ХХК ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Багануур зүүн өмнөд бүсийн цахилгаан түгээх сүлжээ ТӨХК-ийн Дорноговь аймаг дахь салбарын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дорноговь АЗЗА ТӨХК-ийн ажиллагсдын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дорноговь аймгийн Эрүүл мэндийн ажилтны ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Дорноговь аймгийн эм эргэлтийн сангийн ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд сумын нэгдсэн эмнэлгийн ажиллагсадын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Замын-Үүд сумын нэгдсэн эмнэлгийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Сайншанд сумын Зүүнбаян дахь сум дундын эмнэлгийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Уламжлалт анагаах ухаан рашаан сувиллын төвийн ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Өрхийн эмнэлэгүүдийн ажилтнуудын ҮЭХороо' ]);
        Team::create(['team_id'=> $t['id'],'team_type'=>  3, 'is_country'=>  true, 'name'=>  'Борхойн наран өрхийн эрүүл мэндийн төвийн ажилчдын ҮЭХороо' ]);


        // ----------------------------- Завхан аймгийн ҮЭ-ийн холбоо ----------------------------- //
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Аймгийн ЗДТГ-ын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Нэгдсэн эмнэлэгийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Орон нутгийн судлах музейн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Амь-Ус трейд ОНӨААТҮГ-ын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Лабортори Чандмань эрдэнэ сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Жавхлант цогцолбор сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '3-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '4-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Дэвшил сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хот тохижилт ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '1-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '2-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '3-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '4-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '5-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '6-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '7-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '8-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> '9-р цэцэрлэг ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Политехникийн коллеж ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хөгжим бүжгийн коллеж ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хөгжим жүжгийн театер ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Номын сан ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Эрүүл мэндийн газар ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Богдын голын усан цахилгаан станц ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'МУИС завхан сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'НИК ХХК-ний завхан аймаг дахь ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Их-Уул сумын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тосонцэнгэл сумын Нэгдсэн Эмнэлэгийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тосонцэнгэл сумын 1-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тосонцэнгэл сумын 2-р сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тосонцэнгэл сумын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Их-Уул сумын сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Баянтэс сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Түдэвтэй сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Идэр сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цагаанчулуут сумын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цагаанчулуут сумын ЭМТөвийн ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цагаанчулуут сумын ЕБС-ийн ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цагаанчулуут сумын хүүхдийн цэцэрлэгийн ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Нөмрөг сумын ЭМТ-ийн ажилчдын ҮЭ-ийн холбоо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Нөмрөг сумын ЕБС-ийн ажилчдын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Нөмрөг сумын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Баянхайрхан сумын ҮЭХороо' ]);
        Team::create(['team_id'=> $co34['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тэс сумын ЭМТ-ийн ҮЭХороо' ]);

        // ----------------------------- Говь-Алтай аймгийн ҮЭ-ийн холбоо ----------------------------- //
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА-ийн Есөнбулаг сумын ЗДТГ-ын Үйлдвэрчний эвлэлийн хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Ардын дуу бүжгийн Алтай чуулгын ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА-ийн ЗДТГын ажилчдын ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'АШУИС-ийн ГАА дахь АУС-ийн ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА АЗЗА ТӨХК ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА 10 жилийн 1-р дунд сургуулийн ҮЭХороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА Есөнбулаг сум III сургуулийн ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'МСҮТ багш ажилчдын ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Алтай-Улиастай эрчим хүчний системийн дэргэдэх ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ГАА аймгийн нэгдсэн эмнэлгийн ҮЭ хороо' ]);
        Team::create(['team_id'=> $co20['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ус-алтай компанийн ажилчдын ҮЭ хороо' ]);

        // ----------------------------- Архангай аймгийн ҮЭ-ийн холбоо ----------------------------- //
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хүүхдийн цэцэрлэгүүдийн ҮЭ-ийн нэгдсэн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Архангай аймаг МСҮТ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Эрдэнэбулган сумын 2-р сургуулийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Эрдэнэбулган сумын 4-р сургуулийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Ирээдүй цогцолбор сургуулийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Архангай аймгийн багшийн сургуулийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Ус суваг ашиглалтын Ундрага ХХК-ийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хөгжимт драмын театрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Зооноз өвчин судлалын төвийн ҮЭийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Стандарт хэмжил зүйн газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'УЦУОШГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'ХХААГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Эрдэнэбулган сумын 6-р багийн сургууль-цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цахир сумын сургууль цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Цэнхэр сумын эрүүл мэндийн төвийн ҮЭийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хайрхан сумын ЗДТГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хайрхан сумын ЕБС-ийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хайрхан сумын соёлын төвийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хайрхан сумын эрүүл мэндийн төвийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Хайрхан сумын хүүхдийн цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Ихтамир сумын ЗДТГ-ын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Ихтамир сумын хүүхдийн цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Чулуут сумын ЕБС-ийн ҮЭийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Өлзийт сумын ЕБСийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тариат сумын хүүхдийн цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Тариат сумын Мөрөн багийн ЕБС-ийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Эрдэнэмандал сумын ЕБСийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Архангай аймгийн Эрүүл мэндийн газрын ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Батцэнгэл сумын ЕБС-ийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Батцэнгэл сумын Хүүхдийн цэцэрлэгийн ҮЭ-ийн хороо' ]);
        Team::create(['team_id'=> $co16['id'],'team_type'=> 3, 'is_country'=> true, 'name'=> 'Өндөр Улаан сумын Хүүхдийн цэцэрлэгийн ҮЭ-ийн хороо' ]);
    }
}
