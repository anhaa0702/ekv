<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Loyalty;
use App\Model;
use App\LoyaltyUse;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(LoyaltyUse::class, function (Faker $faker) {
    return [
        'qrcode' => $faker->realText(20),
        'loyalty_id' => function() {
            return Loyalty::inRandomOrder()->first()->id;
        },
        'user_id' => function() {
            return User::inRandomOrder()->first()->id;
        },
        'used_date' => null
    ];
});
