<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Loyalty;
use App\Org;
use App\User;
use Faker\Generator as Faker;

$factory->define(Loyalty::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6),
        'type' => $faker->randomElement(['coupon', 'voucher', 'discount']),
        'short_desc' => $faker->realText(25),
        'desc' => $faker->realText(100),
        'main_amount' => $faker->randomNumber(),
        'start_at' => $faker->date(),
        'is_membership' => $faker->randomElement([1, 0]),
        'is_membership_paid' => $faker->randomElement([1, 0]),
        'end_at' => $faker->date(),
        'image' => $faker->imageUrl(),
        'created_user_id' => function() {
            return User::inRandomOrder()->first()->id;
        },
        'created_org_id' => function() {
            return Org::inRandomOrder()->first()->id;
        }
    ];
});
