const mix = require('laravel-mix');

mix.setPublicPath('public_html/');
mix.js('vue/app.js', 'style/js').sass('vue/app.scss', 'style/css');