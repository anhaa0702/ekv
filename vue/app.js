require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'
import { sync } from 'vuex-router-sync'
import VeeValidate from 'vee-validate';
import { ServerTable, ClientTable } from 'vue-tables-2';
import router from './router'
import NProgress from 'vue-nprogress'
import uploadImage from './components/Tools/uploadImage'
import UploadFile from './components/Tools/UploadFile'
import Tinymce from './components/Tools/Tinymce'
import Swatches from 'vue-swatches'
import Multiselect from 'vue-multiselect'
import ScrollLoader from 'vue-scroll-loader'
import VueTextareaAutosize from 'vue-textarea-autosize'
import { BootstrapVue } from 'bootstrap-vue';
import VueYoutube from 'vue-youtube'

import {
    Avatar,
    TreeSelect,
    Rate,
    Breadcrumb,
    InputNumber,
    Steps,
    Modal,
    Upload,
    Button,
    Layout,
    Table,
    Icon,
    Progress,
    Radio,
    Dropdown,
    Menu,
    Carousel,
    Input,
    Calendar,
    Badge,
    Slider,
    Form,
    Tooltip,
    Select,
    Switch,
    Tag,
    Affix,
    Spin,
    Alert,
    Checkbox,
    Tabs,
    Pagination,
    notification,
    Drawer,
    Popconfirm,
    DatePicker,
    Card,
    Cascader,
    Row,
    Col,
    Collapse,
    Divider,
    TimePicker,
    Skeleton,
    List,
    Tree,
} from 'ant-design-vue'

Vue.use(VueRouter);
sync(store, router);
Vue.use(ClientTable, {}, false, 'bootstrap4');
Vue.use(ServerTable, {}, false, 'bootstrap4');
Vue.use(Row)
Vue.use(List)
Vue.use(Col)
Vue.use(Card)
Vue.use(Divider)
Vue.use(TimePicker)
Vue.use(Cascader)
Vue.use(Collapse)
Vue.use(Modal)
Vue.use(Avatar)
Vue.use(Popconfirm)
Vue.use(Button)
Vue.use(Rate)
Vue.use(Tree)
Vue.use(TreeSelect)
Vue.use(Breadcrumb)
Vue.use(Layout)
Vue.use(Table)
Vue.use(Icon)
Vue.use(Progress)
Vue.use(Radio)
Vue.use(Dropdown)
Vue.use(Menu)
Vue.use(Carousel)
Vue.use(Input)
Vue.use(Calendar)
Vue.use(Badge)
Vue.use(Slider)
Vue.use(Form)
Vue.use(Tooltip)
Vue.use(Select)
Vue.use(Tag)
Vue.use(Affix)
Vue.use(Spin)
Vue.use(Alert)
Vue.use(Checkbox)
Vue.use(Tabs)
Vue.use(Pagination)
Vue.use(Upload)
Vue.use(Steps)
Vue.use(InputNumber)
Vue.use(Drawer)
Vue.use(Switch)
Vue.use(notification)
Vue.use(DatePicker)
Vue.use(Card)
Vue.use(Skeleton)
Vue.use(ScrollLoader)
Vue.use(VueTextareaAutosize)
Vue.use(BootstrapVue)
Vue.use(VueYoutube)

Vue.prototype.$notification = notification

VeeValidate.Validator.extend('greater_than_zero', {
    getMessage: field => 'The ' + field + ' field is greater than 0.',
    validate(value, args) { return (parseFloat(value) > 0) ? true : false; },
});

Vue.use(VeeValidate, {
    locale: 'en',
});


Vue.use(NProgress)
Vue.config.productionTip = false
const nprogress = new NProgress({ parent: 'body' })

window.axios.interceptors.request.use(request => {
    if (store.getters.authToken) {
        request.headers.Authorization = `Bearer ${store.getters.authToken}`
    }
    return request
});

window.axios.interceptors.response.use(response => {
    return response;
}, error => {
    const { status } = error.response
    if (status === 404) {
        router.push({ name: '/404' })
    }

    if (status >= 500) {
        alert(error.response.data);
    }

    if (status === 401) {
        store.dispatch('logout')
        router.push({ path: '/login' })
    }

    if (status === 403) {
        alert('Хандах эрхгүй байна!!!');
    }
    return Promise.reject(error)
});

Vue.component('uploadImage', uploadImage);
Vue.component('UploadFile', UploadFile);
Vue.component('Swatches', Swatches);
Vue.component('multiselect', Multiselect);
Vue.component('Tinymce', Tinymce);
Vue.component('SiteApp', require('./App.vue').default);

const app = new Vue({
    el: '#app',
    router,
    store,
    nprogress,
});