import moment from 'moment'
const ekvdate = () => {
    const fromNow = (edate) => {
        const arr = moment(edate).fromNow().split(" ")
        let number = 0
        let msg = 'NaN'
        if (arr[0] === 'a' || arr[0] === 'an') number = 1
        if (arr[0] === 'a' && arr[1] === 'few') number = 'Хэдхэн'
        if (number === 0) number = parseInt(arr[0])

        if (arr[1].includes('second') || arr[2].includes('second')) msg = 'хором'
        else if (arr[1].includes('minute')) msg = 'мин'
        else if (arr[1].includes('hour')) msg = 'цаг'
        else if (arr[1].includes('day')) msg = 'өдөр'
        else if (arr[1].includes('week')) msg = 'долоо хон'
        else if (arr[1].includes('month')) msg = 'сар'
        else if (arr[1].includes('year')) msg = 'жил'
        return `${number} ${msg}`
    }
    return {
        fromNow
    }
}
export default ekvdate