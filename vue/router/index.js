/* eslint-disable no-unused-vars */
import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

//layout
import MainLayout from '../layouts'

// pages
import Login from '../views/public/auth/login'
import Register from '../views/public/auth/register'
import Forget from '../views/public/auth/forgot'
import Partner from '../views/public/auth/partner'
import Terms from '../views/public/help/terms'
import Faqs from '../views/public/help/faqs'
import AboutUs from '../views/public/help/about_us'
import Contact from '../views/public/help/contact'
import IndexPage from '../views/public/index'

import posts from '../views/posts'
import post from '../views/posts/show'

import contents from '../views/contents'
import content from '../views/contents/show'

//user
import profile from '../views/private/profile'

import admins from '../views/admins'
import admin_contents from '../views/admins/contents'
import admin_content_show from '../views/admins/contents/show'
import admin_content_add from '../views/admins/contents/form'
import admin_content_edit from '../views/admins/contents/form'

import admin_content_type from '../views/admins/contents/types'
import admin_content_type_show from '../views/admins/contents/types/show'
import admin_content_type_add from '../views/admins/contents/types/form'
import admin_content_type_edit from '../views/admins/contents/types/form'

import admin_content_authors from '../views/admins/contents/authors'
import admin_content_author_show from '../views/admins/contents/authors/show'
import admin_content_author_add from '../views/admins/contents/authors/form'
import admin_content_author_edit from '../views/admins/contents/authors/form'

import admin_companies from '../views/admins/companies'
import admin_company_show from '../views/admins/companies/show'
import admin_company_add from '../views/admins/companies/form'
import admin_company_edit from '../views/admins/companies/form'

import admin_company_types from '../views/admins/companies/types'
import admin_company_type_show from '../views/admins/companies/types/show'
import admin_company_type_add from '../views/admins/companies/types/form'
import admin_company_type_edit from '../views/admins/companies/types/form'

import admin_users from '../views/admins/users'
import admin_user_show from '../views/admins/users/show'
import admin_user_add from '../views/admins/users/form'
import admin_user_edit from '../views/admins/users/form'


import admin_settings from '../views/admins/settings'
import admin_settings_form from '../views/admins/settings/form'
import admin_settings_show from '../views/admins/settings/show'

import admin_banners from '../views/admins/banners'
import admin_banner_form from '../views/admins/banners/form'
import admin_banner_show from '../views/admins/banners/show'

Vue.use(Router)
let routes = [{
        path: '/home',
        component: MainLayout,
        redirect: '/home',
        children: [
            // index page - Unauthorized
            { path: '/home', component: IndexPage }, // нэвтрээгүй дэлгэц

            { path: '/login', component: Login }, // нэвтрэх
            { path: '/register', component: Register }, // бүртгүүлэх
            { path: '/forgot', component: Forget },

            { path: '/terms', component: Terms },
            { path: '/faqs', component: Faqs },
            { path: '/about_us', component: AboutUs },
            { path: '/contact', component: Contact },
            { path: '/partner', component: Partner },

            { path: '/posts', component: posts },
            { path: '/post/:id', component: post },

            { path: '/contents', component: contents },
            { path: '/contents/:category_id', component: contents },
            { path: '/content/:id', component: content },
        ],
    },
    {
        path: '*',
        redirect: '/home',
    },
    {
        path: '/',
        redirect: '/home',
    },
    {
        path: '/',
        component: MainLayout,
        meta: { requiresAuth: true },
        children: [
            // index page - Authorized
            { path: '/i', component: profile },

            //////////////////////////////////////////////////////////////////////
            //////////////////////// удирлага систем /////////////////////////////
            //////////////////////////////////////////////////////////////////////
            { path: '/admins', component: admins },

            { path: '/admin_contents', component: admin_contents },
            { path: '/admin_content/:id', component: admin_content_show },
            { path: '/admin_content_add', component: admin_content_add },
            { path: '/admin_content/edit/:id', component: admin_content_edit },

            { path: '/admin_content_types', component: admin_content_type },
            { path: '/admin_content_type/:id', component: admin_content_type_show },
            { path: '/admin_content_type_add', component: admin_content_type_add },
            { path: '/admin_content_type/edit/:id', component: admin_content_type_edit },

            { path: '/admin_content_authors', component: admin_content_authors },
            { path: '/admin_content_author/:id', component: admin_content_author_show },
            { path: '/admin_content_author_add', component: admin_content_author_add },
            { path: '/admin_content_author/edit/:id', component: admin_content_author_edit },

            { path: '/admin_companies', component: admin_companies },
            { path: '/admin_company/:id', component: admin_company_show },
            { path: '/admin_company_add', component: admin_company_add },
            { path: '/admin_company/edit/:id', component: admin_company_edit },
            { path: '/admin_company_types', component: admin_company_types },
            { path: '/admin_company_type/:id', component: admin_company_type_show },
            { path: '/admin_company_type_add', component: admin_company_type_add },
            { path: '/admin_company_type/edit/:id', component: admin_company_type_edit },

            { path: '/admin_users', component: admin_users },
            { path: '/admin_user/:id', component: admin_user_show },
            { path: '/admin_user_add', component: admin_user_add },
            { path: '/admin_user/edit/:id', component: admin_user_edit },

            { path: '/admin_settings', component: admin_settings },
            { path: '/admin_setting/:id', component: admin_settings_show },
            { path: '/admin_setting_add', component: admin_settings_form },
            { path: '/admin_setting/edit/:id', component: admin_settings_form },

            { path: '/admin_banners', component: admin_banners },
            { path: '/admin_banner/:id', component: admin_banner_show },
            { path: '/admin_banner_add', component: admin_banner_form },
            { path: '/admin_banner/edit/:id', component: admin_banner_form },
        ],
    },
];

const router = new Router({
    routes,
    scrollBehavior(savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
});
router.beforeEach((to, from, next) => {
    if (from.meta.notloading !== true) {
        if (to.meta.notloading !== true) {
            store.commit('changepageloader', true)
        }
    }
    if (store.getters.authToken && !store.getters.authCheck) {
        store.dispatch('fetchUser').then(() => next());
    } else if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.authCheck) {
        next({ path: '/home' })
    } else {
        next()
    }
});
router.afterEach(() => {
    setTimeout(() => store.commit('changepageloader', false), 1000)
})
export default router;