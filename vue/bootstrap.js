window._ = require('lodash');
window.Popper = require('popper.js').default;
window.moment = require('moment');
window.axios = require('axios');
window.$ = window.jQuery = require('jquery');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = 'web';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}