<?php
use Illuminate\Http\Request;
Route::post('uploadImage',  'UploadController@Image');
Route::post('uploadFile',  'UploadController@File');