<?php
Route::prefix('web')->group(function () {   
    Route::post('const_by_key', 'ConstantData\ConstCtrl@const_by_key');
    Route::post('banners', 'ConstantData\ConstCtrl@banners');
    Route::get('menu', 'ConstantData\ConstCtrl@menu');

    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::post('logout', 'Api\AuthController@logout');
    


    Route::resource('content_types', 'Content\ContentTypeController');
    Route::resource('contents', 'Content\ContentController');

    Route::middleware('auth')->group(function () {
        Route::get('cities', 'ConstantData\ConstCtrl@city');
        Route::get('districts/{id}', 'ConstantData\ConstCtrl@children');
        Route::get('wards/{id}', 'ConstantData\ConstCtrl@children');
    
        Route::get('user', 'Api\AuthController@user');
        
        Route::resource('users', 'User\UserController');
        Route::resource('user_addresses', 'User\UserAddressController');
        Route::resource('user_contacts', 'User\UserContactsController');
        

        Route::resource('posts', 'Content\PostController');
        Route::resource('post_comments', 'Content\CommentController');
        Route::get('post_action/{type}/{id}','Content\PostController@action');

        
        
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////// admin APIs//////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        // байгууллага
        Route::resource('admin_company_types', 'Admins\CompanyTypeController');
        Route::resource('admin_companies', 'Admins\CompanyController');

        // дуу шүлэг контент
        Route::resource('admin_content_types', 'Admins\ContentTypeCtrl');
        Route::resource('admin_content_authors', 'Admins\ContentAuthorCtrl');
        Route::resource('admin_contents', 'Admins\ContentCtrl');
        
        Route::get('getCompanyTypes', 'Admins\CompanyTypeController@getCompanyTypes');
        Route::post('findByNamefromCompany', 'Admins\CompanyController@findByName');
        Route::get('get_bonus_companies', 'Admins\CompanyController@get_bonus_companies');

        Route::resource('admin_users', 'Admins\UserController');
        Route::resource('admin_settings', 'Admins\SettingsController');
        Route::resource('admin_banners', 'Admins\BannerController');

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
    });
});

Route::get('/user/verify/{token}', 'Api\AuthController@verifyEmail');

Route::get('/{vue_capture?}',function(){
    return view('site');
})->where('vue_capture', '[\/\w\.-]*');